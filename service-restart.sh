#!/bin/bash

# 服务重启脚本 

sudo systemctl restart im-database
# sudo systemctl status im-database

sudo systemctl restart im-session
# sudo systemctl status im-session

sudo systemctl restart im-login
# sudo systemctl status im-login

sudo systemctl restart im-gateway
# sudo systemctl status im-gateway
