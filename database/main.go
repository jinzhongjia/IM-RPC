package main

import (
	"IM-RPC/database/flag"
	"IM-RPC/database/service"
	"log"
	"time"

	"github.com/rcrowley/go-metrics"
	"github.com/rpcxio/rpcx-etcd/serverplugin"
	"github.com/smallnest/rpcx/server"
)

func main() {
	d, err := service.Newdb()
	if err != nil {
		log.Fatal("error: ", err)
	}
	s := server.NewServer()
	addRegistryPlugin(s, &flag.Addr, &flag.EtcdAddr, &flag.BasePath)
	s.RegisterName("Db", d, "")
	s.Serve("tcp", flag.Addr)
}

func addRegistryPlugin(s *server.Server, addr *string, etcdAddr *string, basePath *string) {
	r := &serverplugin.EtcdV3RegisterPlugin{
		ServiceAddress: "tcp@" + *addr,
		EtcdServers:    []string{*etcdAddr},
		BasePath:       *basePath,
		Metrics:        metrics.NewRegistry(),
		UpdateInterval: time.Minute,
	}
	err := r.Start()
	if err != nil {
		log.Fatal(err)
	}
	s.Plugins.Add(r)
}
