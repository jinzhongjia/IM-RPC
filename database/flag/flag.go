package flag

import (
	"flag"
	"log"
)

var (
	Addr     string //本机 地址
	EtcdAddr string //etcd 地址
	BasePath string //服务 前缀

	DbType    string //数据库类型
	DbAddress string //数据库地址，类型为mysql时，指ip，类型为sqlite时，指文件地址
	Port      string //端口
	UserName  string //mysql 用户名
	Passwd    string //mysql 密码
	DbName    string //数据库名

	NodeNum int64 //节点序号
	Logger  string
)

func init() {
	flag.StringVar(&Addr, "addr", "localhost:8974", "server address")
	flag.StringVar(&EtcdAddr, "etcdAddr", "localhost:2379", "etcd address")
	flag.StringVar(&BasePath, "base", "/IM", "prefix path")

	flag.StringVar(&DbType, "dbType", "mysql", "db type")
	flag.StringVar(&DbAddress, "dbAddress", "127.0.0.1", "db address or ip")
	flag.StringVar(&Port, "port", "3306", "db port")
	flag.StringVar(&UserName, "userName", "root", "db username")
	flag.StringVar(&Passwd, "passwd", "123456", "db passwd")
	flag.StringVar(&DbName, "dbName", "test", "dbName")
	flag.Int64Var(&NodeNum, "node", 0, "the distributed id")
	flag.StringVar(&Logger, "log", "./gorm.log", "the logger path")
	flag.Parse()
	if NodeNum <= 0 {
		log.Fatal("error: must need the node num!")
	}
}
