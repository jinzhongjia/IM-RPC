package database

import "time"

//获取好友
func (d Db) GetChum(accountA string, accountB string) (Chum, error) {
	var chum Chum
	result := d.D.Where("user_a = ? AND user_b = ?", accountA, accountB).Or("user_a = ? AND user_b = ?", accountB, accountA).Find(&chum)
	return chum, result.Error
}

//获取所有好友
func (d Db) GetAllChum(userA string) ([]Chum, error) {
	var chum []Chum
	result := d.D.Where("user_a = ? ", userA).Or("user_b = ? ", userA).Find(&chum)
	return chum, result.Error
}

// 添加好友
func (d Db) AddChum(userA string, userB string, direction bool) error {
	result := d.D.Create(&Chum{
		UserA:      userA,
		UserB:      userB,
		Direction:  direction,
		CreateTime: time.Now().UnixNano() / 1e6,
	})
	return result.Error
}

// 删除好友
func (d Db) DelChum(userA string, userB string) error {
	// 此处需要进行更改，目前只完成了单向删除功能
	result := d.D.Where("user_a = ? AND user_b = ?", userA, userB).Or("user_a = ? AND user_b = ?", userB, userA).Delete(&Chum{})
	return result.Error
}

//判断好友关系是否存在
func (d Db) ExistChum(userA string, userB string) bool {
	var num int64
	d.D.Model(&Chum{}).Where("user_a = ? AND user_b = ?", userA, userB).Or("user_a = ? AND user_b = ?", userB, userA).Count(&num)
	return num > 0
}
