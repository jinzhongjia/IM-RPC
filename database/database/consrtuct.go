package database

import (
	"IM-RPC/database/flag"
	"errors"
	"fmt"
	"log"
	"strings"

	"github.com/bwmarrin/snowflake"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

//mysql连接
func NewMysql() *gorm.DB {
	dsn := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		flag.UserName,
		flag.Passwd,
		flag.DbAddress,
		flag.Port,
		flag.DbName,
	)
	Db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger: dbLog(),
	})
	if err != nil {
		log.Fatal("connect mysql failed:", err)
	}
	Db.AutoMigrate(&MessageIndex{},
		&MessageContent{},
		&User{},
		&UserData{},
		&Chum{},
		&Group{},
		&GroupMember{},
		&GroupAdmin{},
		&Apply{},
		&NoteIndex{},
		&NoteContent{},
		&FileIndex{},
		&FileContent{},
	)
	return Db
}

//Db的构造函数
func NewDb() (*Db, error) {
	node, err := snowflake.NewNode(flag.NodeNum)
	if err != nil {
		return nil, errors.New(
			strings.Join(
				[]string{"Generate id failed! ", err.Error()},
				"",
			),
		)
	}
	return &Db{
		D:    NewMysql(),
		node: node,
	}, nil
}
