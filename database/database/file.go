package database

import "time"

/*
	id为文件id
*/

//获取文件索引
func (d Db) GetFileIndex(userId string) ([]FileIndex, error) {
	var fileIndex []FileIndex
	result := d.where(
		&FileIndex{
			UserId: userId,
		},
		&fileIndex,
	)
	return fileIndex, result
}

func (d Db) GetFileIndexById(id string) (FileIndex, error) {
	var fileIndex FileIndex
	result := d.where(
		&FileIndex{
			ID: id,
		},
		&fileIndex,
	)
	return fileIndex, result
}

//获取文件信息
func (d Db) GetFileContent(id string) (FileContent, error) {
	var fileContent FileContent
	result := d.where(
		&FileContent{
			ID: id,
		},
		&fileContent,
	)
	return fileContent, result
}

//创建文件记录
func (d Db) CreateFile(userId string, name string, size int64, t string, url string) error {
	//生成随机节点
	id := d.NodeString()
	// 生成时间
	ti := time.Now().UnixNano() / 1e6
	result := d.D.Create(&FileIndex{
		ID:         id,
		UserId:     userId,
		Name:       name,
		Size:       size,
		Type:       t,
		CreateTime: ti,
	})
	if result.Error != nil {
		return result.Error
	}
	result = d.D.Create(&FileContent{
		ID:  id,
		URL: url,
	})
	return result.Error
}

//更新文件名
func (d Db) UpdateFileName(id string, name string) error {
	result := d.D.Model(&FileIndex{}).Where("id = ?", id).Update("name", name)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func (d Db) UpdateFileUrl(id string, url string) error {
	result := d.D.Model(&FileContent{}).Where("id = ?", id).Update("url", url)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

//删除文件
func (d Db) DelFile(id string) error {
	result := d.D.Model(&FileIndex{}).Where("id = ?", id).Delete(&FileIndex{})
	if result.Error != nil {
		return result.Error
	}
	result = d.D.Model(&FileContent{}).Where("id = ?", id).Delete(&FileContent{})
	if result.Error != nil {
		return result.Error
	}
	return nil
}
