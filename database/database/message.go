package database

import (
	"errors"
	"strings"
	"time"

	"gorm.io/gorm"
)

type Message_Index struct {
	ID        string
	AccountA  string //表示队列拥有者，即用户
	AccountB  string //表示另一方
	Direction bool   //表示方向，等于true时表示account_a为消息发送方
	Group     string //表示群组，缺省时表示单聊，不缺省表示群聊
	SendTime  int64
}

type Message_Content struct {
	ID       string
	Type     int8   //消息类型
	Body     string //消息内容
	Extra    string //消息额外信息
	SendTime int64
}

//封装表添加
func (d Db) messageAdd(messageIndex MessageIndex, messageContent MessageContent) error {
	//创建messageindex
	result := d.D.Create(&messageIndex)
	if result.Error != nil {
		return errors.New(
			strings.Join([]string{
				"Create messageIndex failed! ",
				result.Error.Error(),
			},
				"",
			),
		)
	}
	// 创建messagecontent
	result = d.D.Create(&messageContent)
	if result.Error != nil {
		return errors.New(
			strings.Join([]string{
				"Create messageContent failed! ",
				result.Error.Error(),
			},
				"",
			),
		)
	}
	return nil
}

//添加消息
func (d Db) MessageAdd(messageIndex Message_Index, messageContent Message_Content) error {
	time := time.Now().UnixNano() / 1e6
	id := d.Node()
	return d.messageAdd(MessageIndex{
		ID:        id.String(),
		AccountA:  messageIndex.AccountA,
		AccountB:  messageIndex.AccountB,
		Direction: messageIndex.Direction,
		Group:     messageIndex.Group,
		SendTime:  time,
	}, MessageContent{
		ID:       id.String(),
		Type:     messageContent.Type,
		Body:     messageContent.Body,
		Extra:    messageContent.Extra,
		SendTime: time,
	})
}

//查询消息数量
func (d Db) GetMessage(accountA string, num int) ([]Message_Index, []Message_Content, error) {
	var (
		messageIndex   []Message_Index
		messageContent []Message_Content
		// lastTime       int64
	)
	// lastTime, err := d.GetLastTime(accountA)
	// if err != nil {
	// 	return nil, nil, errors.New(
	// 		strings.Join([]string{
	// 			"Get lastTime failed! ",
	// 			err.Error(),
	// 		},
	// 			"",
	// 		),
	// 	)
	// }
	var tmp *gorm.DB
	// if num == 0 {
	// 	tmp = d.D.Where("(account_A = ? OR account_B = ? ) AND send_time > ?", accountA, accountA, lastTime).Order("send_time desc").Table("message_indices")
	// } else {
	// 	tmp = d.D.Where("(account_A = ? OR account_B = ? ) AND send_time > ?", accountA, accountA, lastTime).Order("send_time desc").Limit(num).Table("message_indices")
	// }
	if num == 0 {
		tmp = d.D.Where("account_A = ? OR account_B = ? ", accountA, accountA).Order("send_time desc").Table("message_indices")
	} else {
		tmp = d.D.Where("account_A = ? OR account_B = ? ", accountA, accountA).Order("send_time desc").Limit(num).Table("message_indices")
	}

	if result := tmp.Find(&messageIndex).Error; result != nil {
		return nil, nil, errors.New(
			strings.Join([]string{
				"Get messageIndex failed! ",
				result.Error(),
			},
				"",
			),
		)
	}

	result := d.D.Table("message_contents").Where("id IN ( ? )", tmp.Select("id")).Find(&messageContent)

	if result.Error != nil {
		return nil, nil, errors.New(
			strings.Join([]string{
				"Get messagecontent failed! ",
				result.Error.Error(),
			},
				"",
			),
		)
	}

	return messageIndex, messageContent, nil
}
