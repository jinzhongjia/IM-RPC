package database

import (
	"math/rand"
	"strconv"
	"strings"
	"time"
)

//获取用户
func (d Db) GetUser(account string) (User, error) {
	var user User
	result := d.where(
		&User{
			Account: account,
		},
		&user,
	)
	return user, result
}

// 获取用户信息
func (d Db) GetUserData(id string) (UserData, error) {
	var userData UserData
	result := d.where(
		&UserData{
			ID: id,
		},
		&userData,
	)
	return userData, result
}

//获取最后登录时间
func (d Db) GetLastTime(id string) (int64, error) {
	userData, result := d.GetUserData(id)
	return userData.LastTime, result
}

//新建用户
func (d Db) AddUser(account string, passwd string) error {
	id := d.NodeString()
	result := d.D.Create(&User{
		ID:      id,
		Account: account,
		Passwd:  passwd,
	})
	if result.Error != nil {
		return result.Error
	}
	result = d.D.Create(&UserData{
		ID: id,
		Name: strings.Join(
			[]string{
				"用户",
				strconv.Itoa(
					int(
						rand.New(
							rand.NewSource(
								time.Now().UnixNano(),
							),
						).Int31n(10000),
					),
				),
			},
			"-",
		),
		Sex:        true,
		CreateTime: time.Now().UnixNano() / 1e6,
	})
	return result.Error
}

// 更新密码
func (d Db) UpdatePasswd(id string, passwd string) error {
	result := d.D.Model(&User{}).Where("id = ?", id).Update("passwd", passwd)
	return result.Error
}

// 更新账户
func (d Db) UpdateAccount(id string, account string) error {
	result := d.D.Model(&User{}).Where("id = ?", id).Update("account", account)
	return result.Error
}

// 更新昵称
func (d Db) UpdateName(id string, name string) error {
	result := d.D.Model(&UserData{}).Where("id = ?", id).Update("name", name)
	return result.Error
}

// 更新性别
func (d Db) UpdateSex(id string, sex bool) error {
	result := d.D.Model(&UserData{}).Where("id = ?", id).Update("sex", sex)
	return result.Error
}

//更新最后登录时间
func (d Db) UpdateLastTime(id string) error {
	result := d.D.Model(&UserData{}).Where("id = ?", id).Update("last_time", time.Now().UnixNano()/1e6)
	return result.Error
}

//更新个性签名
func (d Db) UpdateSignature(id string, signature string) error {
	result := d.D.Model(&UserData{}).Where("id = ?", id).Update("signature", signature)
	return result.Error
}

//更新手机号
func (d Db) UpdatePhone(id string, phone int64) error {
	result := d.D.Model(&UserData{}).Where("id = ?", id).Update("phone", phone)
	return result.Error
}

//更新邮箱
func (d Db) UpdateMail(id string, mail string) error {
	result := d.D.Model(&UserData{}).Where("id = ?", id).Update("mail", mail)
	return result.Error
}

//更新个人网站
func (d Db) UpdateSite(id string, site string) error {
	result := d.D.Model(&UserData{}).Where("id = ?", id).Update("site", site)
	return result.Error
}

//更新github
func (d Db) UpdateGithub(id string, github string) error {
	result := d.D.Model(&UserData{}).Where("id = ?", id).Update("github", github)
	return result.Error
}

// 更新QQ
func (d Db) UpdateQq(id string, qq int64) error {
	result := d.D.Model(&UserData{}).Where("id = ?", id).Update("qq", qq)
	return result.Error
}

// 更新weibo
func (d Db) UpdateWeibo(id string, weibo string) error {
	result := d.D.Model(&UserData{}).Where("id = ?", id).Update("weibo", weibo)
	return result.Error
}

// 判断用户id是否存在
func (d Db) UserIdIsExist(id string) bool {
	var num int64
	d.D.Model(&User{}).Where("id = ?", id).Count(&num)
	return num > 0
}

// 判断用户账号是否存在
func (d Db) UserAccountIsExist(account string) bool {
	var num int64
	d.D.Model(&User{}).Where("account = ?", account).Count(&num)
	return num > 0
}
