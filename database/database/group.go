package database

import (
	"errors"
	"strings"
	"time"
)

//创建群组
func (d Db) CreateGroup(name string, ownerId string) error {
	id := d.Node().String()
	// 添加数据库记录
	result := d.D.Create(&Group{
		ID:           id,
		Name:         name,
		Owner:        ownerId,
		HeadPic:      1,
		Time:         time.Now().UnixNano() / 1e6,
		Introduction: "什么介绍都还没有！",
	})

	if result.Error != nil {
		return result.Error
	}
	// 添加群员记录
	err := d.AddGroupMember(id, ownerId)
	if err != nil {
		return err
	}
	// 添加群管理员记录
	err = d.AddGroupAdmin(id, ownerId)
	return err
}

//解散群
func (d Db) DestroyGroup(id string) error {
	if result := d.D.Delete(&Group{
		ID: id,
	}); result.Error != nil {
		return errors.New(
			strings.Join([]string{
				"Destroy group failed! ",
				result.Error.Error(),
			},
				"",
			),
		)
	}

	if result := d.D.Delete(&GroupMember{
		GroupId: id,
	}); result.Error != nil {
		return errors.New(
			strings.Join([]string{
				"Destroy groupMember failed! ",
				result.Error.Error(),
			},
				"",
			),
		)
	}

	if result := d.D.Delete(&GroupAdmin{
		GroupId: id,
	}); result.Error != nil {
		return errors.New(
			strings.Join([]string{
				"Destroy groupAdmin failed! ",
				result.Error.Error(),
			},
				"",
			),
		)
	}
	return nil

}

//获取用户当前的群
func (d Db) GetGroups(userId string) ([]GroupMember, error) {
	var groupMember []GroupMember
	result := d.D.Where(&GroupMember{
		UserId: userId,
	}).Find(&groupMember)
	if result.Error != nil {
		return nil, errors.New(
			strings.Join([]string{
				"Get groups failed! ",
				result.Error.Error(),
			},
				"",
			),
		)
	}
	return groupMember, nil
}

// 获取群组信息
func (d Db) GetGroupInformation(id string) (Group, error) {
	var group Group
	result := d.D.Where(&Group{
		ID: id,
	}).Find(&group)
	return group, result.Error
}

//更新群名
func (d Db) UpdateGroupName(id string, name string) error {
	result := d.D.Model(&Group{
		ID: id,
	}).Update("name", name)
	return result.Error
}

//更新群头像
func (d Db) UpdateGroupHeadPic(id string, headPic int8) error {
	result := d.D.Model(&Group{
		ID: id,
	}).Update("head_pic", headPic)
	return result.Error
}

//转让群操作
func (d Db) UpdateGroupOwner(id string, owner string) error {
	result := d.D.Model(&Group{
		ID: id,
	}).Update("owner", owner)
	return result.Error
}

//更新群简介
func (d Db) UpdateGroupIntro(id string, intro string) error {
	result := d.D.Model(&Group{
		ID: id,
	}).Update("introduction", intro)
	return result.Error
}

//添加群成员
func (d Db) AddGroupMember(groupId string, userId string) error {
	result := d.D.Create(&GroupMember{
		GroupId:  groupId,
		UserId:   userId,
		JoinTime: time.Now().UnixNano() / 1e6,
	})
	return result.Error
}

//获取群所有成员
func (d Db) GetGroupMember(groupId string) ([]GroupMember, error) {
	var groupMember []GroupMember
	result := d.D.Where(&GroupMember{
		GroupId: groupId,
	}).Select("user_id").Find(&groupMember)
	return groupMember, result.Error
}

//获取群员信息，当前只获取到入群时间
func (d Db) GetGroupMemberIntro(groupId string, userId string) (GroupMember, error) {
	var groupMember GroupMember
	result := d.D.Where(&GroupMember{
		GroupId: groupId,
		UserId:  userId,
	}).Find(&groupMember)
	return groupMember, result.Error
}

//删除群成员
func (d Db) DelGroupMember(groupId string, userId string) error {
	result := d.D.Delete(&GroupMember{
		GroupId: groupId,
		UserId:  userId,
	})
	return result.Error
}

// 添加群管理员
func (d Db) AddGroupAdmin(groupId string, userId string) error {
	result := d.D.Create(&GroupAdmin{
		GroupId: groupId,
		UserId:  userId,
		Level:   1,
	})
	return result.Error
}

// 获取群管理员
func (d Db) GetGroupAdmin(groupId string) ([]GroupAdmin, error) {
	var groupAdmin []GroupAdmin
	result := d.D.Where(&GroupAdmin{
		GroupId: groupId,
	}).Find(&groupAdmin)
	return groupAdmin, result.Error
}

// 删除群管理员
func (d Db) DelGroupAdmin(groupId string, userId string) error {
	result := d.D.Delete(&GroupAdmin{
		GroupId: groupId,
		UserId:  userId,
	})
	return result.Error
}

//判断user是否在群里
func (d Db) IsInGroup(groupId string, userId string) bool {
	var num int64
	d.D.Model(&GroupMember{}).Where("group_id = ? AND user_id = ?", groupId, userId).Count(&num)
	return num > 0
}

// 判断groupId是否存在
func (d Db) GroupIsExist(groupId string) bool {
	var num int64
	d.D.Model(&Group{}).Where("id = ?", groupId).Count(&num)
	return num > 0
}
