package database

import "time"

/*

id 为笔记id
*/

//获取所有笔记（笔记索引）
func (d Db) GetNoteIndex(userId string) ([]NoteIndex, error) {
	var noteIndex []NoteIndex
	result := d.where(
		&NoteIndex{
			UserId: userId,
		},
		&noteIndex,
	)
	return noteIndex, result
}

//获取笔记内容
func (d Db) GetNoteContent(id string) (NoteContent, error) {
	var noteContent NoteContent
	result := d.where(
		&NoteContent{
			ID: id,
		},
		&noteContent,
	)
	return noteContent, result
}

// 创建笔记
func (d Db) CreateNote(userId string) error {
	//生成随机节点
	id := d.NodeString()
	// 生成时间
	ti := time.Now().UnixNano() / 1e6
	result := d.D.Create(&NoteIndex{
		ID:         id,
		UserId:     userId,
		CreateTime: ti,
	})
	if result.Error != nil {
		return result.Error
	}
	result = d.D.Create(&NoteContent{
		ID:       id,
		LastTime: ti,
	})
	return result.Error
}

//更新标题
func (d Db) UpdateNoteTitle(id string, title string) error {
	result := d.D.Model(&NoteIndex{}).Where("id = ?", id).Update("title", title)
	if result.Error != nil {
		return result.Error
	}
	err := d.updateNoteLastTime(id)
	return err
}

// 更新内容
func (d Db) UpdateNoteContent(id string, content string) error {
	result := d.D.Model(&NoteContent{}).Where("id = ?", id).Update("content", content)
	if result.Error != nil {
		return result.Error
	}
	err := d.updateNoteLastTime(id)
	return err
}

//更新摘要
func (d Db) UpdateNoteAbstract(id string, abstract string) error {
	result := d.D.Model(&NoteIndex{}).Where("id = ?", id).Update("abstract", abstract)
	if result.Error != nil {
		return result.Error
	}
	err := d.updateNoteLastTime(id)
	return err
}

//更新时间、不暴露
func (d Db) updateNoteLastTime(id string) error {
	result := d.D.Model(&NoteContent{}).Where("id = ?", id).Update("last_time", time.Now().UnixNano()/1e6)
	return result.Error
}

//更新笔记访问密码
func (d Db) UpdateNotePasswd(id string, passwd string) error {
	result := d.D.Model(&NoteContent{}).Where("id = ?", id).Update("passwd", passwd)
	return result.Error
}

//删除笔记
func (d Db) DelNote(id string) error {
	result := d.D.Delete(&NoteIndex{
		ID: id,
	})
	if result.Error != nil {
		return result.Error
	}
	result = d.D.Delete(&NoteContent{
		ID: id,
	})
	return result.Error
}
