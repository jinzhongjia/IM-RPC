/*
对gorm封装
*/
package database

import (
	"log"
	"os"
	"reflect"
	"time"

	"github.com/bwmarrin/snowflake"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type Db struct {
	D    *gorm.DB
	node *snowflake.Node
}

//封装gorm的查询
func (d Db) where(args interface{}, reply interface{}) error {
	if (reflect.ValueOf(args).Kind() != reflect.Ptr) || (reflect.ValueOf(reply).Kind() != reflect.Ptr) {
		panic("the args or reply  is not ptr!")
	}
	result := d.D.Where(args).Find(reply)
	return result.Error
}

//gorm日志记录
func dbLog() logger.Interface {
	return logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer（日志输出的目标，前缀和日志包含的内容——译者注）
		logger.Config{
			SlowThreshold:             time.Second,   // 慢 SQL 阈值
			LogLevel:                  logger.Silent, // 日志级别
			IgnoreRecordNotFoundError: true,          // 忽略ErrRecordNotFound（记录未找到）错误
			Colorful:                  false,         // 禁用彩色打印
		},
	)
}
