package database

import "github.com/bwmarrin/snowflake"

func (d Db) Node() snowflake.ID {
	return d.node.Generate()
}
func (d Db) NodeInt() int64 {
	return d.node.Generate().Int64()
}

func (d Db) NodeString() string {
	return d.node.Generate().String()
}
