package database

import (
	"errors"
	"strings"
	"time"

	"gorm.io/gorm"
)

func (d Db) AddApply(arg Apply) error {

	err := d.D.Create(&Apply{
		ID:       d.NodeString(),
		AccountA: arg.AccountA,
		Type:     arg.Type,
		AccountB: arg.AccountB,
		Group:    arg.Group,
		Deal:     arg.Deal,
		Time:     time.Now().UnixNano() / 1e6,
		Message:  arg.Message,
		Over:     false,
	})
	if err.Error != nil {
		return errors.New(
			strings.Join(
				[]string{
					"add Apply failed!",
					err.Error.Error(),
				},
				"",
			),
		)
	}
	return nil
}

func (d Db) GetApply(accountA string, num int) ([]Apply, error) {
	var apply []Apply
	// lastTime, err := d.GetLastTime(accountA)
	// if err != nil {
	// 	return nil, errors.New(
	// 		strings.Join([]string{
	// 			"Get lastTime failed! ",
	// 			err.Error(),
	// 		},
	// 			"",
	// 		),
	// 	)
	// }
	var result *gorm.DB
	// if num == 0 {
	// 	result = d.D.Where("account_b = ? AND time > ?", accountA, lastTime).Find(&apply)
	// } else {
	// 	result = d.D.Where("account_b = ? AND time > ?", accountA, lastTime).Limit(num).Find(&apply)
	// }
	if num == 0 {
		result = d.D.Debug().Where("account_b = ?", accountA ).Find(&apply)
	} else {
		result = d.D.Debug().Where("account_b = ?", accountA ).Limit(num).Find(&apply)
	}
	if result.Error != nil {
		return nil, errors.New(
			strings.Join([]string{
				"Get lastTime failed! ",
				result.Error.Error(),
			},
				"",
			),
		)
	}
	// fmt.Println("打印请求", apply)
	return apply, nil
}

func (d Db) ApplyOver(id string) {
	d.D.Model(&Apply{}).Where("id = ?", id).Update("over", true)
	// fmt.Println(result.RowsAffected," ",id)
}
