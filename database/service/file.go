package service

import (
	"IM-RPC/database/database"
	"context"
)

type FileIndex struct {
	ID         string `gorm:"primaryKey"` //文件id
	UserId     string //所属用户id
	Name       string //文件名
	Size       int64  //文件大小
	Type       string //文件类型
	CreateTime int64  //创建时间
	URL        string //文件路径
	Passwd     string //访问密码
}

func (d db) GetFileIndex(ctx context.Context, args string, reply *[]database.FileIndex) error {
	var err error
	*reply, err = d.d.GetFileIndex(args)
	return err
}

func (d db) GetFileIndexById(ctx context.Context, args string, reply *database.FileIndex) error {
	var err error
	*reply, err = d.d.GetFileIndexById(args)
	return err
}

func (d db) GetFileContent(ctx context.Context, args string, reply *FileIndex) error {
	result, err := d.d.GetFileContent(args)
	{
		reply.ID = result.ID
		reply.URL = result.URL
	}
	return err
}

func (d db) CreateFile(ctx context.Context, args FileIndex, reply *interface{}) error {
	err := d.d.CreateFile(args.UserId, args.Name, args.Size, args.Type, args.URL)
	return err
}

func (d db) UpdateFileName(ctx context.Context, args FileIndex, reply *interface{}) error {
	err := d.d.UpdateFileName(args.ID, args.Name)
	return err
}

func (d db) UpdateFileUrl(ctx context.Context, args FileIndex, reply *interface{}) error {
	err := d.d.UpdateFileUrl(args.ID, args.URL)
	return err
}

func (d db) DelFile(ctx context.Context, args string, reply *interface{}) error {
	err := d.d.DelFile(args)
	return err
}
