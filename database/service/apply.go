package service

import (
	"IM-RPC/database/database"
	"context"
)

type ApplyParam struct {
	AccountA string
	Num      int
}

func (d db) AddApply(ctx context.Context, args database.Apply, reply *interface{}) error {
	err := d.d.AddApply(args)
	return err
}

func (d db) GetApply(ctx context.Context, args ApplyParam, reply *[]database.Apply) error {
	var err error
	// fmt.Println(args)
	*reply, err = d.d.GetApply(args.AccountA, args.Num)
	return err
}

func (d db) ApplyOver(ctx context.Context, args string, reply *interface{}) error {
	d.d.ApplyOver(args)
	return nil
}
