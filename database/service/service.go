package service

import (
	"IM-RPC/database/database"
)

type db struct {
	d *database.Db
}

func Newdb() (*db, error) {
	d, err := database.NewDb()
	if err != nil {
		return nil, err
	}
	return &db{
		d: d,
	}, nil
}
