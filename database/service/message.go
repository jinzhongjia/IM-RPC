package service

import (
	"IM-RPC/database/database"
	"context"
)

type GetIntro struct {
	AccountA string
	Num      int
}

type Message struct {
	AccountA  string //表示队列拥有者，即用户
	AccountB  string //表示另一方
	Direction bool   //表示方向，等于true时表示account_a为消息发送方
	// MessageId int64  //表示消息内容ID，即t_message_content表中的id
	Group    string //表示群组，缺省时表示单聊，不缺省表示群聊
	Type     int8   //消息类型
	Body     string //消息内容
	Extra    string //消息额外信息
	SendTime int64
}

func (d db) MessageAdd(ctx context.Context, args Message, reply *interface{}) error {
	err := d.d.MessageAdd(database.Message_Index{
		AccountA:  args.AccountA,
		AccountB:  args.AccountB,
		Direction: args.Direction,
		Group:     args.Group,
	}, database.Message_Content{
		Type:  args.Type,
		Body:  args.Body,
		Extra: args.Extra,
	})
	// pushError(err)
	return err
}

func (d db) GetMessage(ctx context.Context, args GetIntro, reply *[]Message) error {
	*reply = make([]Message, 0)
	index, content, err := d.d.GetMessage(args.AccountA, args.Num)
	//map存储
	tmp := make(map[string]*Message)
	for _, v := range index {
		tmp[v.ID] = &Message{
			AccountA:  v.AccountA,
			AccountB:  v.AccountB,
			Direction: v.Direction,
			Group:     v.Group,
		}

	}
	for _, v := range content {
		tmp[v.ID].Type = v.Type
		tmp[v.ID].Body = v.Body
		tmp[v.ID].Extra = v.Extra
		tmp[v.ID].SendTime = v.SendTime
	}
	for _, v := range tmp {
		*reply = append(*reply, *v)
	}
	// pushError(err)
	return err
}
