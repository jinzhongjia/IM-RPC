package service

import (
	"IM-RPC/database/database"
	"context"
)

type NoteIndex struct {
	ID         string `gorm:"primaryKey"` //笔记id
	Title      string //标题
	Abstract   string //摘要
	UserId     string //用户id
	CreateTime int64  //创建时间
}

type NoteContent struct {
	ID       string `gorm:"primaryKey"` //笔记id
	Content  string //内容
	Passwd   string //访问密码
	LastTime int64  //上次修改时间
}

func (d db) GetNoteIndex(ctx context.Context, args string, reply *[]database.NoteIndex) error {
	var err error
	*reply, err = d.d.GetNoteIndex(args)

	return err
}

func (d db) GetNoteContent(ctx context.Context, args string, reply *NoteContent) error {
	result, err := d.d.GetNoteContent(args)
	{
		reply.ID = result.ID
		reply.Content = result.Content
		reply.Passwd = result.Passwd
		reply.LastTime = result.LastTime
	}
	return err
}

func (d db) CreateNote(ctx context.Context, args string, reply *interface{}) error {
	err := d.d.CreateNote(args)
	return err
}

func (d db) UpdateNoteTitle(ctx context.Context, args NoteIndex, reply *interface{}) error {
	err := d.d.UpdateNoteTitle(args.ID, args.Title)
	return err
}

func (d db) UpdateNoteContent(ctx context.Context, args NoteContent, reply *interface{}) error {
	err := d.d.UpdateNoteContent(args.ID, args.Content)
	return err
}

func (d db) UpdateNoteAbstract(ctx context.Context, args NoteIndex, reply *interface{}) error {
	err := d.d.UpdateNoteAbstract(args.ID, args.Abstract)
	return err
}

func (d db) UpdateNotePasswd(ctx context.Context, args NoteContent, reply *interface{}) error {
	err := d.d.UpdateNotePasswd(args.ID, args.Passwd)
	return err
}

func (d db) DelNote(ctx context.Context, args string, reply *interface{}) error {
	err := d.d.DelNote(args)
	return err
}
