package service

import (
	"IM-RPC/database/database"
	"context"
)

type Group struct {
	ID           string `gorm:"primaryKey"` //群组ID
	Name         string //群名
	HeadPic      int8   //群头像
	Owner        string //群拥有者
	Introduction string //群介绍
	Time         int64  //创建时间
}

type GroupMember struct {
	GroupId  string //群号
	UserId   string //用户id
	JoinTime int64  //加入群聊时间
}

type GroupAdmin struct {
	GroupId string //群号
	UserId  string //用户id
	Level   int8   //管理员权限等级
}

func (d db) CreateGroup(ctx context.Context, args Group, reply *interface{}) error {
	err := d.d.CreateGroup(args.Name, args.Owner)
	// pushError(err)
	return err
}

func (d db) DestroyGroup(ctx context.Context, args string, reply *interface{}) error {
	err := d.d.DestroyGroup(args)
	// pushError(err)
	return err
}

func (d db) GetGroups(ctx context.Context, args string, reply *[]database.GroupMember) error {
	var err error
	*reply, err = d.d.GetGroups(args)
	// pushError(err)
	return err
}

func (d db) GetGroupInformation(ctx context.Context, args string, reply *Group) error {
	tmp, err := d.d.GetGroupInformation(args)
	*reply = Group{
		ID:           tmp.ID,
		Name:         tmp.Name,
		HeadPic:      tmp.HeadPic,
		Owner:        tmp.Owner,
		Introduction: tmp.Introduction,
		Time:         tmp.Time,
	}
	// pushError(err)
	return err
}

func (d db) UpdateGroupName(ctx context.Context, args Group, reply *interface{}) error {
	err := d.d.UpdateGroupName(args.ID, args.Name)
	// pushError(err)
	return err
}

func (d db) UpdateGroupHeadPic(ctx context.Context, args Group, reply *interface{}) error {
	err := d.d.UpdateGroupHeadPic(args.ID, args.HeadPic)
	// pushError(err)
	return err
}

func (d db) UpdateGroupOwner(ctx context.Context, args Group, reply *interface{}) error {
	err := d.d.UpdateGroupOwner(args.ID, args.Owner)
	// pushError(err)
	return err
}

func (d db) UpdateGroupIntro(ctx context.Context, args Group, reply *interface{}) error {
	err := d.d.UpdateGroupIntro(args.ID, args.Introduction)
	// pushError(err)
	return err
}

func (d db) AddGroupMember(ctx context.Context, args GroupMember, reply *interface{}) error {
	err := d.d.AddGroupMember(args.GroupId, args.UserId)
	// pushError(err)
	return err
}

func (d db) GetGroupMember(ctx context.Context, args string, reply *[]database.GroupMember) error {
	var err error
	*reply, err = d.d.GetGroupMember(args)
	// pushError(err)
	return err
}

func (d db) GetGroupMemberIntro(ctx context.Context, args GroupMember, reply *GroupMember) error {

	tmp, err := d.d.GetGroupMemberIntro(args.GroupId, args.UserId)
	*reply = GroupMember{
		GroupId:  tmp.GroupId,
		UserId:   tmp.UserId,
		JoinTime: tmp.JoinTime,
	}
	// pushError(err)
	return err
}

func (d db) DelGroupMember(ctx context.Context, args GroupMember, reply *interface{}) error {
	err := d.d.DelGroupMember(args.GroupId, args.UserId)
	// pushError(err)
	return err
}

func (d db) AddGroupAdmin(ctx context.Context, args GroupAdmin, reply *interface{}) error {
	err := d.d.AddGroupAdmin(args.GroupId, args.UserId)
	// pushError(err)
	return err
}

func (d db) GetGroupAdmin(ctx context.Context, args string, reply *[]database.GroupAdmin) error {
	var err error
	*reply, err = d.d.GetGroupAdmin(args)
	// pushError(err)
	return err
}

func (d db) DelGroupAdmin(ctx context.Context, args GroupAdmin, reply *interface{}) error {
	err := d.d.DelGroupAdmin(args.GroupId, args.UserId)
	// pushError(err)
	return err
}

func (d db) IsInGroup(ctx context.Context, args GroupMember, reply *bool) error {
	*reply = d.d.IsInGroup(args.GroupId, args.UserId)
	return nil
}

func (d db) GroupIsExist(ctx context.Context, args string, reply *bool) error {
	*reply = d.d.GroupIsExist(args)
	return nil
}
