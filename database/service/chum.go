package service

import (
	"IM-RPC/database/database"

	"golang.org/x/net/context"
)

type ChumGroup struct {
	ID     int64
	Name   string
	UserId string
}

type Chum struct {
	GroupId   int64  //好友组id
	UserA     string //用户a
	UserB     string //用户b
	Direction bool   //当Direction为true时，表示usera为发送方
	// CreateTime int64  //好友添加时间
}

func (d db) GetChum(ctx context.Context, args Chum, reply *database.Chum) error {
	var err error
	*reply, err = d.d.GetChum(args.UserA, args.UserB)
	// pushError(err)
	return err
}

func (d db) GetAllChum(ctx context.Context, args string, reply *[]database.Chum) error {
	var err error
	*reply, err = d.d.GetAllChum(args)
	// pushError(err)
	return err
}

func (d db) AddChum(ctx context.Context, args Chum, reply *interface{}) error {
	err := d.d.AddChum(args.UserA, args.UserB, args.Direction)
	// pushError(err)
	return err
}

func (d db) DelChum(ctx context.Context, args Chum, reply *interface{}) error {
	err := d.d.DelChum(args.UserA, args.UserB)
	// pushError(err)
	return err
}

func (d db) ExistChum(ctx context.Context, args Chum, reply *bool) error {
	*reply = d.d.ExistChum(args.UserA, args.UserB)
	return nil
}
