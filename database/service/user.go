package service

import (
	"context"
)

type User struct {
	ID      string //用户主键，使用分布式全局主键写入
	Account string //用户名
	Passwd  string //密码hash
}

type UserData struct {
	ID         string //用户主键，使用分布式全局主键写入,这里需要和user主键相同
	Name       string //用户昵称
	Sex        bool   //性别
	CreateTime int64  //账户创建时间
	LastTime   int64  //最后登录时间
	Signature  string //个性签名
	Phone      int64  //手机号
	Mail       string //邮件
	Site       string //个人网站
	Github     string //github
	Qq         int64  //QQ号
	Weibo      string //微博
}

func (d db) GetUser(ctx context.Context, args string, reply *User) error {

	tmp, err := d.d.GetUser(args)
	*reply = User{
		ID:      tmp.ID,
		Account: tmp.Account,
		Passwd:  tmp.Passwd,
	}
	// pushError(err)
	return err
}

func (d db) GetUserData(ctx context.Context, args string, reply *UserData) error {

	tmp, err := d.d.GetUserData(args)
	// fmt.Println(tmp)
	*reply = UserData{
		ID:         tmp.ID,
		Name:       tmp.Name,
		Sex:        tmp.Sex,
		CreateTime: tmp.CreateTime,
		LastTime:   tmp.LastTime,
		Signature:  tmp.Signature,
		Phone:      tmp.Phone,
		Mail:       tmp.Mail,
		Site:       tmp.Site,
		Github:     tmp.Github,
		Qq:         tmp.Qq,
		Weibo:      tmp.Weibo,
	}
	// pushError(err)
	return err
}

func (d db) GetLastTime(ctx context.Context, args string, reply *int64) error {
	var err error
	*reply, err = d.d.GetLastTime(args)
	// pushError(err)
	return err
}

func (d db) AddUser(ctx context.Context, args User, reply *interface{}) error {
	err := d.d.AddUser(args.Account, args.Passwd)
	// pushError(err)
	return err
}

func (d db) UpdatePasswd(ctx context.Context, args User, reply *interface{}) error {
	err := d.d.UpdatePasswd(args.ID, args.Passwd)
	// pushError(err)
	return err
}

func (d db) UpdateAccount(ctx context.Context, args User, reply *interface{}) error {
	err := d.d.UpdateAccount(args.ID, args.Account)
	// pushError(err)
	return err
}

func (d db) UpdateName(ctx context.Context, args UserData, reply *interface{}) error {
	err := d.d.UpdateName(args.ID, args.Name)
	// pushError(err)
	return err
}

func (d db) UpdateSex(ctx context.Context, args UserData, reply *interface{}) error {
	err := d.d.UpdateSex(args.ID, args.Sex)
	// pushError(err)
	return err
}

func (d db) UpdateLastTime(ctx context.Context, args string, reply *interface{}) error {
	err := d.d.UpdateLastTime(args)
	// pushError(err)
	return err
}

func (d db) UpdateSignature(ctx context.Context, args UserData, reply *interface{}) error {
	err := d.d.UpdateSignature(args.ID, args.Signature)
	// pushError(err)
	return err
}

func (d db) UpdatePhone(ctx context.Context, args UserData, reply *interface{}) error {
	err := d.d.UpdatePhone(args.ID, args.Phone)
	return err
}

func (d db) UpdateMail(ctx context.Context, args UserData, reply *interface{}) error {
	err := d.d.UpdateMail(args.ID, args.Mail)
	return err
}

func (d db) UpdateSite(ctx context.Context, args UserData, reply *interface{}) error {
	err := d.d.UpdateSite(args.ID, args.Site)
	return err
}

func (d db) UpdateGithub(ctx context.Context, args UserData, reply *interface{}) error {
	err := d.d.UpdateGithub(args.ID, args.Github)
	return err
}

func (d db) UpdateQq(ctx context.Context, args UserData, reply *interface{}) error {
	err := d.d.UpdateQq(args.ID, args.Qq)
	return err
}

func (d db) UpdateWeibo(ctx context.Context, args UserData, reply *interface{}) error {
	err := d.d.UpdateWeibo(args.ID, args.Weibo)
	return err
}

func (d db) UserIdIsExist(ctx context.Context, args string, reply *bool) error {
	*reply = d.d.UserIdIsExist(args)
	return nil
}

func (d db) UserAccountIsExist(ctx context.Context, args string, reply *bool) error {
	*reply = d.d.UserAccountIsExist(args)
	return nil
}
