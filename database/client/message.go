package client

func (c Client) MessageAdd(message Message) {
	c.handle("MessageAdd", message, nil)
}

func (c Client) GetMessage(id string, num int) []Message {
	var message []Message
	c.handle("GetMessage", GetIntro{
		AccountA: id,
		num:      num,
	}, &message)
	return message
}
