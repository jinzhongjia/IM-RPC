package client

func (c Client) CreateGroup(name string, ownerId string) {
	c.handle("CreateGroup", Group{
		Name:  name,
		Owner: ownerId,
	}, nil)
}

func (c Client) DestroyGroup(id string) {
	c.handle("DestroyGroup", id, nil)
}

func (c Client) GetGroups(userId string) []GroupMember {
	var groupMember []GroupMember
	c.handle("GetGroups", userId, &groupMember)
	return groupMember
}

func (c Client) GetGroupInformation(groupId string) Group {
	var group Group
	c.handle("GetGroupInformation", groupId, &group)
	return group
}

func (c Client) UpdateGroupName(groupId string, name string) {
	c.handle("UpdateGroupName", Group{
		ID:   groupId,
		Name: name,
	}, nil)
}

func (c Client) UpdateGroupHeadPic(groupId string, headPic int8) {
	c.handle("UpdateGroupHeadPic", Group{
		ID:      groupId,
		HeadPic: headPic,
	}, nil)
}

func (c Client) UpdateGroupOwner(groupId string, owner string) {
	c.handle("UpdateGroupOwner", Group{
		ID:    groupId,
		Owner: owner,
	}, nil)
}

func (c Client) UpdateGroupIntro(groupId string, introduction string) {
	c.handle("UpdateGroupIntro", Group{
		ID:           groupId,
		Introduction: introduction,
	}, nil)
}

func (c Client) AddGroupMember(groupId string, userId string) {
	c.handle("AddGroupMember", GroupMember{
		GroupId: groupId,
		UserId:  userId,
	}, nil)
}

func (c Client) GetGroupMember(groupId string) []GroupMember {
	var groupMember []GroupMember
	c.handle("GetGroupMember", groupId, &groupMember)
	return groupMember
}

func (c Client) GetGroupMemberIntro(groupId string, userId string) GroupMember {
	var groupMember GroupMember
	c.handle("GetGroupMemberIntro", GroupMember{
		GroupId: groupId,
		UserId:  userId,
	}, &groupMember)
	return groupMember
}

func (c Client) DelGroupMember(groupId string, userId string) {
	c.handle("DelGroupMember", GroupMember{
		GroupId: groupId,
		UserId:  userId,
	}, nil)
}

func (c Client) AddGroupAdmin(groupId string, userId string) {
	c.handle("AddGroupAdmin", GroupAdmin{
		GroupId: groupId,
		UserId:  userId,
	}, nil)
}

func (c Client) GetGroupAdmin(groupId string) []GroupAdmin {
	var groupAdmin []GroupAdmin
	c.handle("GetGroupAdmin", groupId, &groupAdmin)
	return groupAdmin
}

func (c Client) DelGroupAdmin(groupId string, userId string) {
	c.handle("DelGroupAdmin", GroupAdmin{
		GroupId: groupId,
		UserId:  userId,
	}, nil)
}

func (c Client) IsInGroup(groupId string, userId string) bool {
	var result bool
	c.handle("IsInGroup", GroupMember{
		GroupId: groupId,
		UserId:  userId,
	}, &result)
	return result
}

func (c Client) GroupIsExist(groupId string) bool {
	var result bool
	c.handle("GroupIsExist", groupId, &result)
	return result
}
