package client

func (c Client) GetUser(account string) User {
	var user User
	c.handle("GetUser", account, &user)
	return user
}

func (c Client) GetUserData(id string) UserData {
	var userData UserData
	c.handle("GetUserData", id, &userData)
	// fmt.Println(userData)
	return userData
}

func (c Client) GetLastTime(id string) int64 {
	var lastTime int64
	c.handle("GetLastTime", id, &lastTime)
	return lastTime
}

func (c Client) AddUser(account string, passwd string) {
	c.handle("AddUser", User{
		Account: account,
		Passwd:  passwd,
	}, nil)
}

func (c Client) UpdatePasswd(id string, passwd string) {
	c.handle("UpdatePasswd", User{
		ID:     id,
		Passwd: passwd,
	}, nil)
}

func (c Client) UpdateAccount(id string, account string) {
	c.handle("UpdateAccount", User{
		ID:      id,
		Account: account,
	}, nil)
}

func (c Client) UpdateName(id string, name string) {
	c.handle("UpdateName", UserData{
		ID:   id,
		Name: name,
	}, nil)
}

func (c Client) UpdateSex(id string, sex bool) {
	c.handle("UpdateSex", UserData{
		ID:  id,
		Sex: sex,
	}, nil)
}

func (c Client) UpdateLastTime(id string) {
	c.handle("UpdateLastTime", id, nil)
}

func (c Client) UpdateSignature(id string, signature string) {
	c.handle("UpdateSignature", UserData{
		ID:        id,
		Signature: signature,
	}, nil)
}

func (c Client) UpdatePhone(id string, phone int64) {
	c.handle("UpdatePhone", UserData{
		ID:    id,
		Phone: phone,
	}, nil)
}

func (c Client) UpdateMail(id string, mail string) {
	c.handle("UpdateMail", UserData{
		ID:   id,
		Mail: mail,
	}, nil)
}

func (c Client) UpdateSite(id string, site string) {
	c.handle("UpdateSite", UserData{
		ID:   id,
		Site: site,
	}, nil)
}

func (c Client) UpdateGithub(id string, github string) {
	c.handle("UpdateGithub", UserData{
		ID:     id,
		Github: github,
	}, nil)
}

func (c Client) UpdateQq(id string, qq int64) {
	c.handle("UpdateQq", UserData{
		ID: id,
		Qq: qq,
	}, nil)
}

func (c Client) UpdateWeibo(id string, weibo string) {
	c.handle("UpdateWeibo", UserData{
		ID:    id,
		Weibo: weibo,
	}, nil)
}

func (c Client) UserIdIsExist(id string) bool {
	var result bool
	c.handle("UserIdIsExist", id, &result)
	return result
}

func (c Client) UserAccountIsExist(account string) bool {
	var result bool
	c.handle("UserAccountIsExist", account, &result)
	return result
}
