package client

type Apply struct {
	ID       string `gorm:"primaryKey"` //申请消息id
	AccountA string //消息发出者
	Type     int8   //消息类型,这里使用二进制操作，一共三位二进制，操作第一位代表好友还是群组，二三位10申请、11同意、00拒绝、01代表消息被处理过
	AccountB string //消息接受者
	Group    string //是否为群申请
	Deal     string //群组申请处理人
	Time     int64  //发送时间
	Message  string //消息
	Over     bool
}

type Chum struct {
	UserA      string //用户a
	UserB      string //用户b
	Direction  bool   //当Direction为true时，表示usera为发送方
	CreateTime int64  //好友添加时间
}

type Group struct {
	ID           string `gorm:"primaryKey"` //群组ID
	Name         string //群名
	HeadPic      int8   //群头像
	Owner        string //群拥有者
	Introduction string //群介绍
	Time         int64  //创建时间
}

type GroupMember struct {
	GroupId  string //群号
	UserId   string //用户id
	JoinTime int64  //加入群聊时间
}

type GroupAdmin struct {
	GroupId string //群号
	UserId  string //用户id
	Level   int8   //管理员权限等级
}

type GetIntro struct {
	AccountA string
	num      int
}

type User struct {
	ID      string //用户主键，使用分布式全局主键写入
	Account string //用户名
	Passwd  string //密码hash
}

type UserData struct {
	ID         string //用户主键，使用分布式全局主键写入,这里需要和user主键相同
	Name       string //用户昵称
	Sex        bool   //性别
	CreateTime int64  //账户创建时间
	LastTime   int64  //最后登录时间
	Signature  string //个性签名
	Phone      int64  //手机号
	Mail       string //邮件
	Site       string //个人网站
	Github     string
	Qq         int64
	Weibo      string
}

type Message struct {
	AccountA  string //表示队列拥有者，即用户
	AccountB  string //表示另一方
	Direction bool   //表示方向，等于true时表示account_a为消息发送方
	// MessageId int64  //表示消息内容ID，即t_message_content表中的id
	Group    string //表示群组，缺省时表示单聊，不缺省表示群聊
	Type     int8   //消息类型
	Body     string //消息内容
	Extra    string //消息额外信息
	SendTime int64
}

type NoteIndex struct {
	ID         string `gorm:"primaryKey"` //笔记id
	Title      string //标题
	Abstract   string //摘要
	UserId     string //用户id
	CreateTime int64  //创建时间
}

type NoteContent struct {
	ID       string `gorm:"primaryKey"` //笔记id
	Content  string //内容
	Passwd   string //访问密码
	LastTime int64  //上次修改时间
}

type FileIndex struct {
	ID         string `gorm:"primaryKey"` //文件id
	UserId     string //所属用户id
	Name       string //文件名
	Size       int64  //文件大小
	Type       string //文件类型
	CreateTime int64  //创建时间
	URL        string //文件路径
	Passwd     string //访问密码
}

type File struct {
	ID         string `gorm:"primaryKey"` //文件id
	UserId     string //所属用户id
	Name       string //文件名
	Size       int64  //文件大小
	Type       string //文件类型
	CreateTime int64  //创建时间
}

type ApplyParam struct {
	AccountA string
	Num      int
}
