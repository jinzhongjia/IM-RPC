package client

func (c Client) AddApply(accountA string, Type int8, accountB string, group string, deal string, message string) {
	c.handle("AddApply", Apply{
		AccountA: accountA,
		Type:     Type,
		AccountB: accountB,
		Group:    group,
		Deal:     deal,
		Message:  message,
	}, nil)
}

func (c Client) GetApply(accountA string, num int) []Apply {
	var apply []Apply
	//这里使用一个匿名结构体
	c.handle("GetApply", ApplyParam{
		AccountA: accountA,
		Num:      num,
	}, &apply)
	// fmt.Println(accountA)
	return apply
}

func (c Client) ApplyOver(id string) {
	c.handle("ApplyOver", id, nil)
}
