package client

func (c Client) GetChum(userA string, userB string) Chum {
	var chum Chum
	c.handle("GetChum", Chum{
		UserA: userA,
		UserB: userB,
	}, &chum)
	return chum
}

func (c Client) GetAllChum(id string) []Chum {
	var chum []Chum
	c.handle("GetAllChum", id, &chum)
	return chum
}

func (c Client) AddChum(userA string, userB string, direction bool) {

	c.handle("AddChum", Chum{
		UserA:     userA,
		UserB:     userB,
		Direction: direction,
	}, nil)
}

func (c Client) DelChum(userA string, userB string) {
	c.handle("DelChum", Chum{
		UserA: userA,
		UserB: userB,
	}, nil)
}

func (c Client) ExistChum(userA string, userB string) bool {
	var result bool
	c.handle("ExistChum", Chum{
		UserA: userA,
		UserB: userB,
	}, &result)
	return result
}
