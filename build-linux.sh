#!/bin/bash

#在docker中执行

# 删除编译出来的文件
rm im-rpc-database
rm im-rpc-session
rm im-rpc-login
rm im-rpc-chat
rm im-rpc-gateway
rm im-rpc-router

# 编译database模块
go build -ldflags="-s -w" -o im-rpc-database ./database && upx im-rpc-database -9

# 编译session模块
go build -ldflags="-s -w" -o im-rpc-session ./session && upx im-rpc-session -9

# 编译login模块
go build -ldflags="-s -w" -o im-rpc-login ./login && upx im-rpc-login -9

# 编译chat模块
go build -ldflags="-s -w" -o im-rpc-chat ./chat && upx im-rpc-chat -9

# 编译gateway模块
go build -ldflags="-s -w" -o im-rpc-gateway ./gateway && upx im-rpc-gateway -9

# 编译router模块
go build -ldflags="-s -w" -o im-rpc-router ./router && upx im-rpc-router -9