package main

import (
	"IM-RPC/gateway/flag"
	"IM-RPC/gateway/serverRegister"
	"IM-RPC/gateway/tool"

	chatClient "IM-RPC/chat/client"
	websocket "IM-RPC/gateway/websocket/core"
	loginClient "IM-RPC/login/client"
	"strconv"

	"github.com/gin-gonic/gin"
)

func main() {
	//注册服务gateway服务
	register, err := serverRegister.NewServiceRegister([]string{flag.EtcdAddr}, 5)
	if err != nil {
		panic(err)
	}
	// 结束后注销服务
	defer register.Close()

	// 创建login连接句柄
	lc := loginClient.NewClient([]string{flag.EtcdAddr}, flag.BasePath)

	// 创建chat连接句柄
	cl := chatClient.NewClient([]string{flag.EtcdAddr}, flag.BasePath)

	// 设置gin状态为发行版
	gin.SetMode(gin.ReleaseMode)
	// 创建gin的engine
	r := gin.Default()
	// 设置上传最大限制
	r.MaxMultipartMemory = int64(flag.MaxUpSize) << 20
	// 跨域中间件
	r.Use(tool.Cors)

	//权限认证中间件
	r.Use(func(c *gin.Context) {
		tool.Authorization(lc, c)
	})

	//websocket管理
	{
		hub := websocket.New(cl, lc)

		r.GET("/ws", func(c *gin.Context) {
			hub.ServeWs(c.Writer, c.Request)
		})
	}

	// Http处理
	{
		// 账号注册
		account(lc, r)
		// 用户信息
		user(lc, r)
		//笔记
		note(lc, r)
		//文件
		file(lc, r)
		// 好友
		chum(lc, r)
		// 群组处理
		group(lc,r)
		// 消息处理
		msg(lc, r)
		//申请处理
		apply(lc, r)
	}

	// 运行服务
	r.Run("127.0.0.1:" + strconv.Itoa(flag.Port))
}
