package serverRegister

import (
	"IM-RPC/gateway/flag"
	"IM-RPC/gateway/tool"
	"context"
	"log"
	"time"

	clientv3 "go.etcd.io/etcd/client/v3"
)

//ServiceRegister 创建租约注册服务
type ServiceRegister struct {
	cli     *clientv3.Client //etcd client
	leaseID clientv3.LeaseID //租约ID
	//租约keepalieve相应chan
	keepAliveChan <-chan *clientv3.LeaseKeepAliveResponse
	ctl           chan int
	conNum        int32
}

//NewServiceRegister 新建注册服务
func NewServiceRegister(endpoints []string, lease int64) (*ServiceRegister, error) {

	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   endpoints,
		DialTimeout: 5 * time.Second,
	})
	if err != nil {
		return nil, err
	}

	// 构造结构体
	ser := &ServiceRegister{
		cli:    cli,
		conNum: 0,
		ctl:    make(chan int),
	}

	//申请租约设置时间keepalive
	if err := ser.putKeyWithLease(lease); err != nil {
		return nil, err
	}

	return ser, nil
}

//设置租约
func (s *ServiceRegister) putKeyWithLease(lease int64) error {
	//设置租约时间
	resp, err := s.cli.Grant(context.Background(), lease)
	if err != nil {
		return err
	}

	data, err := tool.Marshal(0)
	if err != nil {
		panic("err:marshal data failed")
	}
	//注册服务并绑定租约
	_, err = s.cli.Put(context.Background(), flag.RegisterPath+flag.ExposeAddr, string(data), clientv3.WithLease(resp.ID))
	if err != nil {
		return err
	}
	//设置续租 定期发送需求请求
	leaseRespChan, err := s.cli.KeepAlive(context.Background(), resp.ID)

	if err != nil {
		return err
	}
	s.leaseID = resp.ID
	// log.Println(s.leaseID)
	s.keepAliveChan = leaseRespChan
	// log.Printf("Put key:%s  val:%s  success!", s.key, s.val)
	go s.listenLeaseRespChan()
	go s.listen()
	return nil
}

//ListenLeaseRespChan 监听 续租情况
func (s *ServiceRegister) listenLeaseRespChan() {

Loop:
	for {
		select {
		case _, ok := <-s.keepAliveChan:
			if !ok {
				break Loop
			}
		}

	}

	log.Println("关闭续租")

}

// Close 注销服务
func (s *ServiceRegister) Close() error {
	//撤销租约
	if _, err := s.cli.Revoke(context.Background(), s.leaseID); err != nil {
		return err
	}
	return s.cli.Close()
}

func (s *ServiceRegister) Increase() {
	s.ctl <- 1
}

func (s *ServiceRegister) Reduce() {
	s.ctl <- 0
}

func (s *ServiceRegister) listen() {
	for v := range s.ctl {
		if v == 0 {
			s.conNum--
		} else {
			s.conNum++
		}

		data, err := tool.Marshal(s.conNum)
		if err != nil {
			log.Println("error: marshal failed in listen", err)
			continue
		}
		//注册服务并绑定租约
		_, err = s.cli.Put(context.Background(), flag.RegisterPath+flag.ExposeAddr, string(data), clientv3.WithLease(s.leaseID))
		if err != nil {
			log.Println("error: etcd put failed in listen", err)
			continue
		}
	}
}
