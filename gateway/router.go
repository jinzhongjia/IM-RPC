package main

import (
	"IM-RPC/gateway/http"
	loginClient "IM-RPC/login/client"

	"github.com/gin-gonic/gin"
)

// 账号注册
func account(lc *loginClient.Client, r *gin.Engine) {
	http.Account(lc, r)
}

// 用户信息
func user(lc *loginClient.Client, r *gin.Engine) {
	http.User(lc, r)
}

//笔记
func note(lc *loginClient.Client, r *gin.Engine) {
	http.Note(lc, r)
}

//文件
func file(lc *loginClient.Client, r *gin.Engine) {
	http.File(lc, r)
}

// 好友
func chum(lc *loginClient.Client, r *gin.Engine) {
	http.Chum(lc, r)
}

func group(lc *loginClient.Client, r *gin.Engine) {
	http.Group(lc, r)
}

// 消息处理
func msg(lc *loginClient.Client, r *gin.Engine) {
	http.Msg(lc, r)
}

//申请处理
func apply(lc *loginClient.Client, r *gin.Engine) {
	http.Apply(lc, r)
}
