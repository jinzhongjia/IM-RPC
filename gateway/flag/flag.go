package flag

import (
	"flag"
	"log"
	"os"
	"path/filepath"
)

var (
	Port         int    //本机 地址
	EtcdAddr     string //etcd 地址
	BasePath     string //服务 前缀
	RegisterPath string //gateway注册服务前缀
	ExposeAddr   string
	Weight       int
	SessionId    string
	MaxUpSize    int
	UploadPath   string
	SitePath     string
)

var UPath string

const (
	Succeed = "succeed!"
	Failed  = "failed!"
)

func init() {
	flag.IntVar(&Port, "addr", 8976, "server port")
	flag.StringVar(&EtcdAddr, "etcdAddr", "localhost:2379", "etcd address")
	flag.StringVar(&BasePath, "base", "/IM", "prefix path")
	flag.StringVar(&RegisterPath, "register", "/IM/gateWay", "the register path")
	flag.StringVar(&ExposeAddr, "expose", "http://api.jinzh.me/", "expose addr")
	flag.IntVar(&Weight, "weight", 1, "gateway weight")
	flag.StringVar(&SessionId, "sessionid", "Session-Id", "sessionid in header")
	flag.IntVar(&MaxUpSize, "maxUpSize", 32, "max upload size")
	flag.StringVar(&UploadPath, "uploadPath", "G:/project/go/Im-RPC/upload", "upload path")
	flag.StringVar(&SitePath, "sitePath", "G:/project/go/Im-RPC", "site path")
	flag.Parse()
	if !filepath.IsAbs(UploadPath) || !filepath.IsAbs(SitePath) {
		//绝对地址
		log.Fatal("uploadPath must be absolute path")
	}
	//获取上传文件对外暴露相对路径
	UPath, _ = filepath.Rel(SitePath, UploadPath)
	// 判断路径是否存在
	_, err := os.Stat(UploadPath)
	if err != nil && os.IsNotExist(err) {
		os.MkdirAll(UploadPath, os.ModePerm)
	}
}
