package tool

import (
	"IM-RPC/gateway/flag"
	loginClient "IM-RPC/login/client"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Cors(c *gin.Context) {
	//跨域处理
	method := c.Request.Method
	origin := c.Request.Header.Get("Origin") //请求头部
	if origin != "" {
		//接收客户端发送的origin （重要！）
		c.Writer.Header().Set("Access-Control-Allow-Origin", origin)
		//服务器支持的所有跨域请求的方法
		c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE,UPDATE")
		//允许跨域设置可以返回其他子段，可以自定义字段
		c.Header("Access-Control-Allow-Headers", "Authorization, Content-Length, X-CSRF-Token, Token,session,X_Requested_With,Accept, Origin, Host, Connection, Accept-Encoding, Accept-Language,DNT, X-CustomHeader, Keep-Alive, User-Agent, X-Requested-With, If-Modified-Since, Cache-Control, Content-Type, Pragma,Session-Id")
		// 允许浏览器（客户端）可以解析的头部 （重要）
		c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers")
		//设置缓存时间
		c.Header("Access-Control-Max-Age", "172800")
		//允许客户端传递校验信息比如 cookie (重要)
		c.Header("Access-Control-Allow-Credentials", "true")
	}

	//允许类型校验
	if method == "OPTIONS" {
		c.JSON(http.StatusOK, "ok!")
	}

	defer func() {
		if err := recover(); err != nil {
			log.Printf("Panic info is: %v", err)
		}
	}()
}

func Authorization(lc *loginClient.Client, c *gin.Context) {
	url := c.Request.URL.Path
	if url == "/account/register" || url == "/account/login" || url == "/ws" || url[:1+len(flag.UPath)] == "/"+flag.UPath {

	} else {
		sessionId := c.GetHeader("Session-Id")
		if !lc.Authorization(sessionId) {
			c.String(http.StatusUnauthorized, "you have not login in")

			log.Println("An unauthenticated user attempted to login, ip: ", c.ClientIP())
			// 此处尝试获取客户端真实ip，如果使用了反向代理，如nginx，需要额外进行配置
			// proxy_set_header X-Real-IP $remote_addr;
			// proxy_set_header X-Forward-For $remote_addr;
			c.Abort()
		}
	}
}
