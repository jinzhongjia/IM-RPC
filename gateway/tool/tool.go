package tool

import (
	"IM-RPC/gateway/flag"
	"IM-RPC/public/GatewayProto"

	"github.com/golang/protobuf/proto"
)

func Marshal(conNum int32) ([]byte, error) {
	status := &GatewayProto.Status{
		ConNum: conNum,
		Weight: int32(flag.Weight),
	}
	data, err := proto.Marshal(status)
	if err != nil {
		return make([]byte, 0), err
	}
	return data, nil
}
