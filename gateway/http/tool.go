package http

import (
	"IM-RPC/gateway/flag"

	"github.com/gin-gonic/gin"
)

func getSessionId(c *gin.Context) string {
	return c.GetHeader(flag.SessionId)
}
