package http

import (
	"IM-RPC/gateway/flag"
	loginClient "IM-RPC/login/client"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Note(lc *loginClient.Client, r *gin.Engine) {
	note := r.Group("/note")
	{
		//获取笔记索引
		getNoteIndex(lc, note)
		// 获取笔记内容
		getNoteContent(lc, note)
		// 创建笔记
		createNote(lc, note)
		// 修改笔记标题
		updateNoteTitle(lc, note)
		// 修改笔记内容
		updateNoteContent(lc, note)
		// 修改笔记密码
		updateNotePasswd(lc, note)
		// 删除笔记
		delNote(lc, note)
		// 分享笔记
		note.POST("/share", func(ctx *gin.Context) {})
	}
}

//获取笔记索引
func getNoteIndex(lc *loginClient.Client, note *gin.RouterGroup) {
	note.GET("/index", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		noteIndex := lc.GetNoteIndex(id)
		c.JSON(http.StatusOK, noteIndex)
	})
}

// 获取笔记内容
func getNoteContent(lc *loginClient.Client, note *gin.RouterGroup) {
	note.POST("/content", func(c *gin.Context) {
		// sessionId := getSessionId(c)
		// id := lc.StI(sessionId)
		noteId := c.PostForm("noteId")
		noteContent := lc.GetNoteContent(noteId)
		c.JSON(http.StatusOK, noteContent)
	})
}

// 创建笔记
func createNote(lc *loginClient.Client, note *gin.RouterGroup) {
	note.POST("/create", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		lc.CreateNote(id)
		c.String(http.StatusOK, flag.Succeed)
	})
}

// 修改笔记标题
func updateNoteTitle(lc *loginClient.Client, note *gin.RouterGroup) {
	note.POST("/updateTitle", func(c *gin.Context) {
		// sessionId := getSessionId(c)
		// id := lc.StI(sessionId)
		noteId := c.PostForm("noteId")
		noteTitle := c.PostForm("noteTitle")
		lc.UpdateNoteTitle(noteId, noteTitle)
		c.String(http.StatusOK, flag.Succeed)
	})
}

// 修改笔记内容
func updateNoteContent(lc *loginClient.Client, note *gin.RouterGroup) {
	note.POST("/updateContent", func(c *gin.Context) {
		noteId := c.PostForm("noteId")
		noteContent := c.PostForm("noteContent")
		lc.UpdateNoteContent(noteId, noteContent)
		c.String(http.StatusOK, flag.Succeed)
	})
}

// 修改密码
func updateNotePasswd(lc *loginClient.Client, note *gin.RouterGroup) {
	note.POST("/updatePasswd", func(c *gin.Context) {
		noteId := c.PostForm("noteId")
		notePasswd := c.PostForm("notePasswd")
		lc.UpdateNotePasswd(noteId, notePasswd)
		c.String(http.StatusOK, flag.Succeed)
	})
}

// 删除笔记
func delNote(lc *loginClient.Client, note *gin.RouterGroup) {
	note.POST("/del", func(c *gin.Context) {
		noteId := c.PostForm("noteId")
		lc.DelNote(noteId)
		c.String(http.StatusOK, flag.Succeed)
	})
}
