package http

import (
	loginClient "IM-RPC/login/client"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Chum(lc *loginClient.Client, r *gin.Engine) {
	chum := r.Group("/chum")
	{
		// 获取好友信息（加好友时间这种）
		getChum(lc, chum)
		//获取所有好友列表
		getAllChum(lc, chum)
	}
}

func getChum(lc *loginClient.Client, chum *gin.RouterGroup) {
	chum.POST("/getOne", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		userId := c.PostForm("userId")
		chum := lc.GetChum(id, userId)
		c.JSON(http.StatusOK, chum)
	})
}

func getAllChum(lc *loginClient.Client, chum *gin.RouterGroup) {
	chum.GET("/get", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		allChum := lc.GetAllChum(id)
		c.JSON(http.StatusOK, allChum)

	})
}
