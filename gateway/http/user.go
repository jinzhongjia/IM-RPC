package http

import (
	"IM-RPC/gateway/flag"
	loginClient "IM-RPC/login/client"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func User(lc *loginClient.Client, r *gin.Engine) {

	user := r.Group("/user")
	{
		//获取用户所有信息
		info(lc, user)
		//更新昵称
		updateName(lc, user)
		// 更新性别
		updateSex(lc, user)
		//更新签名
		updateSignature(lc, user)
		//更新电话
		updatePhone(lc, user)
		//更新mail
		updateMail(lc, user)
		//更新个人网站
		updateSite(lc, user)
		//更新用户名
		updateAccount(lc, user)
		//更新密码
		updatePasswd(lc, user)
		//更新github
		updateGithub(lc, user)
		//更新QQ
		updateQq(lc, user)
		//更新微博
		updateWeibo(lc, user)
		getUserData(lc, user)
	}

}

//获取用户所有信息
func info(lc *loginClient.Client, user *gin.RouterGroup) {
	user.GET("/info", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		userData := lc.GetUserData(id)
		c.JSON(http.StatusOK, userData)
	})
}

//更新昵称，不可为空
func updateName(lc *loginClient.Client, user *gin.RouterGroup) {
	user.POST("/updateName", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		name := c.PostForm("name")
		if name == "" {
			c.String(http.StatusBadRequest, flag.Failed+"昵称不能为空")
			return
		}
		lc.UpdateName(id, name)
		c.String(http.StatusOK, flag.Succeed)
	})
}

// 更新性别，不可为空
func updateSex(lc *loginClient.Client, user *gin.RouterGroup) {
	user.POST("/updateSex", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		sexV := c.PostForm("sex")
		var sex bool
		if sexV == "1" {
			// 女性
			sex = true
		} else if sexV == "0" {
			// 男性
			sex = false
		} else {
			c.String(http.StatusBadRequest, flag.Failed+"性别参数不正确")
			return
		}
		lc.UpdateSex(id, sex)
		c.String(http.StatusOK, flag.Succeed)
	})
}

//更新签名，可以为空
func updateSignature(lc *loginClient.Client, user *gin.RouterGroup) {
	user.POST("/updateSignature", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		signature := c.PostForm("signature")
		lc.UpdateSignature(id, signature)
		c.String(http.StatusOK, flag.Succeed)
	})
}

//更新电话，可以为空
func updatePhone(lc *loginClient.Client, user *gin.RouterGroup) {
	user.POST("/updatePhone", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		phoneV := c.PostForm("phone")
		if phoneV == "" {
			c.String(http.StatusBadRequest, flag.Failed+"电话不能为空")
			return
		}
		phone, err := strconv.Atoi(phoneV)
		if err != nil {
			c.String(http.StatusBadRequest, flag.Failed+err.Error())
			return
		}
		// fmt.Println(phone)
		lc.UpdatePhone(id, int64(phone))
		c.String(http.StatusOK, flag.Succeed)
	})
}

//更新mail，可以为空
func updateMail(lc *loginClient.Client, user *gin.RouterGroup) {
	user.POST("/updateMail", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		mail := c.PostForm("mail")
		if mail == "" {
			c.String(http.StatusBadRequest, flag.Failed+"邮箱不能为空")
			return
		}
		lc.UpdateMail(id, mail)
		c.String(http.StatusOK, flag.Succeed)
	})
}

//更新个人网站，可以为空
func updateSite(lc *loginClient.Client, user *gin.RouterGroup) {
	user.POST("/updateSite", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		site := c.PostForm("site")
		lc.UpdateSite(id, site)
		c.String(http.StatusOK, flag.Succeed)
	})
}

//更新用户名，不可为空
func updateAccount(lc *loginClient.Client, user *gin.RouterGroup) {
	user.POST("/updateAccount", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		account := c.PostForm("account")
		result := lc.UpdateAccount(id, account)
		if !result {
			c.String(http.StatusBadRequest, flag.Failed)
		} else {
			c.String(http.StatusOK, flag.Succeed)
		}
	})
}

// 更新密码
func updatePasswd(lc *loginClient.Client, user *gin.RouterGroup) {
	user.POST("/updatePasswd", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		passwd := c.PostForm("passwd")
		result := lc.UpdatePasswd(id, passwd)
		if !result {
			c.String(http.StatusBadRequest, flag.Failed)
		} else {
			c.String(http.StatusOK, flag.Succeed)
		}
	})
}

//更新github，可以为空
func updateGithub(lc *loginClient.Client, user *gin.RouterGroup) {
	user.POST("/updateGithub", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		github := c.PostForm("github")
		lc.UpdateGithub(id, github)
		c.String(http.StatusOK, flag.Succeed)
	})
}

// 更新QQ，可以为空
func updateQq(lc *loginClient.Client, user *gin.RouterGroup) {
	user.POST("/updateQq", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		qqV := c.PostForm("qq")
		qq, err := strconv.Atoi(qqV)
		if err != nil {
			c.String(http.StatusBadRequest, flag.Failed)
			return
		}
		lc.UpdateQq(id, int64(qq))
		c.String(http.StatusOK, flag.Succeed)
	})
}

// 更新微博，可以为空
func updateWeibo(lc *loginClient.Client, user *gin.RouterGroup) {
	user.POST("/updateWeibo", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		weibo := c.PostForm("weibo")
		lc.UpdateWeibo(id, weibo)
		c.String(http.StatusOK, flag.Succeed)
	})
}

//获取某个用户信息
func getUserData(lc *loginClient.Client, user *gin.RouterGroup) {
	user.POST("/getData", func(c *gin.Context) {
		userId := c.PostForm("userId")
		userData := lc.GetUserData(userId)
		c.JSON(http.StatusOK, userData)
	})
}
