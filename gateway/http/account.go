package http

import (
	"IM-RPC/gateway/flag"
	loginClient "IM-RPC/login/client"
	"net/http"

	"github.com/gin-gonic/gin"
)

/*
	1.功能概括：登录、登出、注册、忘记密码
	2.未完成：忘记密码
*/

func Account(lc *loginClient.Client, r *gin.Engine) {
	account := r.Group("/account")
	{
		//登录
		login(lc, account)
		// 登出
		logOut(lc, account)
		// 注册
		register(lc, account)
		// 忘记密码
		forget(lc, account)
		// 授权
		auth(lc, account)
	}
}

//登录
func login(lc *loginClient.Client, account *gin.RouterGroup) {
	account.POST("/login", func(c *gin.Context) {
		account := c.PostForm("account")
		passwd := c.PostForm("passwd")
		sessionId, err := lc.Login(account, passwd)
		if err != nil {
			c.String(http.StatusNotAcceptable, flag.Failed+err.Error())
		} else {
			c.String(http.StatusOK, sessionId)
		}
	})
}

// 注销
func logOut(lc *loginClient.Client, account *gin.RouterGroup) {
	account.POST("/logout", func(c *gin.Context) {
		sessionId := c.GetHeader(flag.SessionId)
		lc.LogOut(sessionId)
		c.String(http.StatusOK, flag.Succeed)
	})
}

// 注册
func register(lc *loginClient.Client, account *gin.RouterGroup) {
	account.POST("/register", func(c *gin.Context) {
		account := c.PostForm("account")
		passwd := c.PostForm("passwd")
		if lc.Register(account, passwd) {
			c.String(http.StatusOK, flag.Succeed)
		} else {
			c.String(http.StatusNotAcceptable, flag.Failed)
		}
	})
}

//忘记密码
func forget(lc *loginClient.Client, account *gin.RouterGroup) {
	account.POST("/forget", func(ctx *gin.Context) {})
}

func auth(lc *loginClient.Client, account *gin.RouterGroup) {
	account.GET("/auth", func(ctx *gin.Context) {
		sessionId := ctx.GetHeader(flag.SessionId)
		if lc.Authorization(sessionId) {
			ctx.String(http.StatusOK, flag.Succeed)
		} else {
			ctx.String(http.StatusForbidden, flag.Failed)
		}
	})
}
