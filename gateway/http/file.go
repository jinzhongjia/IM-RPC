package http

import (
	"IM-RPC/gateway/flag"
	"IM-RPC/gateway/tool"
	loginClient "IM-RPC/login/client"
	"fmt"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func File(lc *loginClient.Client, r *gin.Engine) {
	r.Static("/"+flag.UPath, flag.UploadPath)
	//设置上传文件最大值

	file := r.Group("/file")
	{
		//获取文件列表
		getFileIndex(lc, file)
		// 获取文件、下载文件
		getFileContent(lc, file)
		// 上传文件
		uploadFile(lc, file)
		// 获取上传文件大小最大值
		// file.GET("/uploadMax", func(ctx *gin.Context) {})
		// 更新文件名
		updateFileName(lc, file)
		// 删除文件
		delFile(lc, file)
		// 分享文件
		// file.POST("/share", func(ctx *gin.Context) {})
	}
}

func getFileIndex(lc *loginClient.Client, file *gin.RouterGroup) {
	file.GET("/index", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		fileIndex := lc.GetFileIndex(id)
		c.JSON(http.StatusOK, fileIndex)
	})
}

func getFileContent(lc *loginClient.Client, file *gin.RouterGroup) {
	file.POST("/content", func(c *gin.Context) {
		// sessionId := getSessionId(c)
		// id := lc.StI(sessionId)
		fileId := c.PostForm("fileId")
		fileContent := lc.GetFileContent(fileId)
		c.JSON(http.StatusOK, fileContent)
	})
}

func uploadFile(lc *loginClient.Client, file *gin.RouterGroup) {
	file.POST("/upload", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		//获取上传的文件
		file, err := c.FormFile("file")
		if err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintf("get err %s", err.Error()))
			return
		}

		//判断上传的文件是否存在
		_, err = os.Stat(flag.UploadPath + "/" + id + "-" + file.Filename)
		if err == nil {
			c.String(http.StatusNotAcceptable, flag.Failed)
			return
		}
		f, err := file.Open()
		if err != nil {
			c.String(http.StatusInternalServerError, flag.Failed)
			return
		}
		// 获取文件类型
		fileType, err := tool.GetFileContentType(f)
		if err != nil {
			c.String(http.StatusInternalServerError, flag.Failed)
		}
		//保存文件
		c.SaveUploadedFile(file, flag.UploadPath+"/"+id+"-"+file.Filename)
		//数据库存储数据

		lc.CreateFile(id, file.Filename, file.Size, fileType, flag.ExposeAddr+flag.UPath+"/"+id+"-"+file.Filename)
		c.String(http.StatusOK, flag.Succeed)
	})
}

func updateFileName(lc *loginClient.Client, file *gin.RouterGroup) {
	file.POST("/updateName", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)

		fileId := c.PostForm("fileId")
		fileName := c.PostForm("fileName")
		fileIndex := lc.GetFileIndexById(fileId)
		lc.UpdateFileName(fileId, fileName)
		lc.UpdateFileUrl(fileId, flag.ExposeAddr+flag.UPath+"/"+id+"-"+fileName)
		os.Rename(flag.UploadPath+"/"+id+"-"+fileIndex.Name, flag.UploadPath+"/"+id+"-"+fileName)
		c.String(http.StatusOK, flag.Succeed)
	})
}

func delFile(lc *loginClient.Client, file *gin.RouterGroup) {
	file.POST("/del", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		fileId := c.PostForm("fileId")
		fileIndex := lc.GetFileIndexById(fileId)
		if os.Remove(flag.UploadPath+"/"+id+"-"+fileIndex.Name) != nil {
			c.String(http.StatusBadRequest, flag.Failed)
			return
		}
		lc.DelFile(fileId)
		c.String(http.StatusOK, flag.Succeed)
	})
}
