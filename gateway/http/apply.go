package http

import (
	"IM-RPC/gateway/flag"
	loginClient "IM-RPC/login/client"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func Apply(lc *loginClient.Client, r *gin.Engine) {
	apply := r.Group("/apply")
	{
		// 最近的申请
		recentApply(lc, apply)

		ApplyOver(lc, apply)
	}
}

func recentApply(lc *loginClient.Client, apply *gin.RouterGroup) {
	apply.POST("/recent", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		numV := c.PostForm("num")
		num, err := strconv.Atoi(numV)
		if err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintf("err %s", err.Error()))
		}
		reply := lc.RecentApply(id, num)
		c.JSON(http.StatusOK, reply)
	})
}

func ApplyOver(lc *loginClient.Client, apply *gin.RouterGroup) {
	apply.POST("/over", func(c *gin.Context) {
		applyId := c.PostForm("applyId")
		// fmt.Println(applyId)
		lc.ApplyOver(applyId)
		c.String(http.StatusOK, flag.Succeed)
	})
}
