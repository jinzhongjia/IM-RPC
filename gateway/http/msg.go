package http

import (
	loginClient "IM-RPC/login/client"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func Msg(lc *loginClient.Client, r *gin.Engine) {
	msg := r.Group("/msg")
	{
		// 最近的消息
		recentMsg(lc, msg)
	}
}

func recentMsg(lc *loginClient.Client, msg *gin.RouterGroup) {

	msg.POST("/recent", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		numV := c.PostForm("num")
		num, err := strconv.Atoi(numV)
		if err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintf("err %s", err.Error()))
		}
		msg := lc.RecentMsg(id, num)
		c.JSON(http.StatusOK, msg)

	})
}
