package http

import (
	"IM-RPC/gateway/flag"
	loginClient "IM-RPC/login/client"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Group(lc *loginClient.Client, r *gin.Engine) {
	group := r.Group("/group")
	{
		// 创建群组
		createGroup(lc, group)
		// 获取所有群组
		getGroups(lc, group)
		// 获取群组信息
		getGroupInformation(lc, group)
	}
}

func createGroup(lc *loginClient.Client, group *gin.RouterGroup) {
	group.POST("/create", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		name := c.PostForm("name")
		lc.CreateGroup(name, id)
		c.String(http.StatusOK, flag.Succeed)
	})
}

func getGroups(lc *loginClient.Client, group *gin.RouterGroup) {
	group.GET("/getAll", func(c *gin.Context) {
		sessionId := getSessionId(c)
		id := lc.StI(sessionId)
		groups := lc.GetGroups(id)
		c.JSON(http.StatusOK, groups)
	})
}

func getGroupInformation(lc *loginClient.Client, group *gin.RouterGroup) {
	group.POST("/getInfo", func(c *gin.Context) {
		groupId := c.PostForm("groupId")
		groupData := lc.GetGroupInformation(groupId)
		c.JSON(http.StatusOK, groupData)
	})
}
