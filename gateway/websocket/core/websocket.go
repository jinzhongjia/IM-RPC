package core

import (
	Cl "IM-RPC/chat/client"
	"IM-RPC/gateway/flag"
	loginClient "IM-RPC/login/client"
	"log"
	"net/http"
)

func New(chat *Cl.Client, lc *loginClient.Client) *Hub {
	hub := newHub(chat, lc)
	go hub.run()
	go hub.Send()
	return hub
}

func (hub *Hub) ServeWs(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	// 获取sessionId
	sessionId := r.URL.Query().Get(flag.SessionId)
	// sessionId转为id
	id := hub.lc.StI(sessionId)
	// fmt.Println("有新连接，sessionId：", sessionId, "id：", id)
	if id == "" {
		// 传入无效的sessionid
		// log.Println("传入无效的sessionid", sessionId)
		return
	}
	// 向chat执行注册操作
	hub.Register(id)
	// 创建一个client
	client := &Client{
		SessionId: sessionId,
		hub:       hub,
		conn:      conn,
		send:      make(chan []byte, 256),
	}
	// 向hub注册client
	client.hub.register <- client

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()
}
