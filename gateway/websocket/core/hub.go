package core

import (
	Cl "IM-RPC/chat/client"
	loginClient "IM-RPC/login/client"
	"fmt"
)

type Hub struct {
	// Registered clients.
	clients map[string]*Client

	// Inbound messages from the clients.
	broadcast chan []byte

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client

	Chat *Cl.Client

	lc *loginClient.Client
}

func newHub(chat *Cl.Client, lc *loginClient.Client) *Hub {
	return &Hub{
		broadcast:  make(chan []byte),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[string]*Client),
		Chat:       chat,
		lc:         lc,
	}
}

func (h *Hub) run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client.SessionId] = client
		case client := <-h.unregister:
			if _, ok := h.clients[client.SessionId]; ok {
				delete(h.clients, client.SessionId)
				close(client.send)
			}
		case message := <-h.broadcast:
			for _, client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, client.SessionId)
				}
			}
		}
	}
}

// 单独开启协程运行
func (h *Hub) Send() {
	for msg := range h.Chat.Ch {
		id := msg.Metadata["id"]
		sessionId := h.lc.Its(id)
		fmt.Println("准备回送消息sessionId:", sessionId, "id:", id)
		data := msg.Payload
		fmt.Println(h.clients)
		if client, ok := h.clients[sessionId]; ok {
			fmt.Println("触发消息回送")
			select {
			case client.send <- data:
			default:
				close(client.send)
				delete(h.clients, client.SessionId)
				h.UnRegister(id)
			}
		} else {
			// 没有用户取消注册
			fmt.Println("没有该用户")
			h.UnRegister(id)
		}
	}

}

// 封装好的注册
func (h *Hub) Register(id string) {
	h.Chat.Register(id)
}

// 封装好的取消注册
func (h *Hub) UnRegister(id string) {
	h.Chat.UnRegister(id)
}
