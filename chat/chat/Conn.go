package chat

import (
	"golang.org/x/sync/syncmap"
)

type Conn struct {
	Data syncmap.Map
}

func NewConn() *Conn {
	return &Conn{
		Data: syncmap.Map{},
	}
}

// 存储
func (c *Conn) Set(key string, value interface{}) {
	c.Data.Store(key, value)
}

// 读取
func (c *Conn) Get(key string) (interface{}, bool) {
	return c.Data.Load(key)
}

// 删除
func (c *Conn) Delete(key string) {
	c.Data.Delete(key)
}
