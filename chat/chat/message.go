package chat

import (
	"IM-RPC/chat/tool"
	databaseClient "IM-RPC/database/client"
	"IM-RPC/public/wsProto"
	"fmt"
	"log"
	"net"
	"time"
)

// 消息泵
func (c *Chat) Pump(data []byte, sender string) error {
	newBody, err := tool.UnMarshal(data)
	if err != nil {
		return err
	}
	fmt.Println("消息处理")
	///加入信息判断逻辑，处理信息
	c.deal(newBody, sender)
	return nil
}

func (c *Chat) deal(newBody *wsProto.Body, sender string) {
	fmt.Println(newBody)
	if newBody.Type == 1 {
		if newBody.GetMsg().GetGroupId() == "" {
			//单聊处理
			c.dealSingle(newBody.GetMsg(), sender)
		} else {
			//群聊处理
			c.dealGroup(newBody.GetMsg(), sender)
		}
	} else if newBody.Type == 2 {
		if newBody.GetApply().GetGroupId() == "" {
			//单人好友申请
			fmt.Println("触发了单人好友申请")
			c.dealSingleApply(newBody.GetApply(), sender)
		} else {
			// 加群申请
			c.dealGroupApply(newBody.GetApply(), sender)
		}
	} else {
		log.Println("非法数据")
	}
}

// 注册
func (c *Chat) Register(id string, conn net.Conn) {
	c.C.Set(id, conn)
}

// 取消注册
func (c *Chat) UnRegister(id string) (interface{}, bool) {
	return c.C.Get(id)
}

// 发送信息
func (c *Chat) send(id string, msg []byte) {
	conn, exist := c.C.Get(id)
	if !exist {
		return
	}
	err := c.s.SendMessage(
		conn.(net.Conn),
		"",
		"",
		map[string]string{"id": id},
		msg)
	if err != nil {
		log.Println("error: ", err)
	}
}

// 单聊处理
func (c *Chat) dealSingle(msg *wsProto.Msg, sender string) {
	// 如果id不存在，这里直接返回不进行任何处理
	if !c.Dc.UserIdIsExist(msg.GetEnd()) {
		return
	}
	newBody := &wsProto.Body{
		Type: 1,
		Msg: &wsProto.Msg{
			End:   sender,
			Extra: msg.Extra,
			Type:  msg.Type,
			Msg:   msg.Msg,
			Time:  time.Now().UnixNano() / 1e6,
		},
	}
	fmt.Println("打印end:", msg.End)
	data, err := tool.Marshal(newBody)
	if err != nil {
		log.Println("此处出错,", err)
		return
	}
	c.send(msg.End, data)

	//数据库存储
	c.Dc.MessageAdd(databaseClient.Message{
		AccountA:  sender,
		AccountB:  msg.End,
		Direction: true,
		Type:      int8(msg.Type),
		Body:      msg.Msg,
		Extra:     msg.Extra,
		SendTime:  time.Now().UnixNano() / 1e6,
	})
}

// 处理群聊
func (c *Chat) dealGroup(msg *wsProto.Msg, sender string) {
	// 判断groupId是否存在，不存在直接return
	if !c.Dc.GroupIsExist(msg.GetGroupId()) {
		return
	}
	newBody := &wsProto.Body{
		Type: 1,
		Msg: &wsProto.Msg{
			End:     sender,
			GroupId: msg.GroupId,
			Extra:   msg.Extra,
			Type:    msg.Type,
			Msg:     msg.Msg,
			Time:    time.Now().UnixNano() / 1e6,
		},
	}
	data, err := tool.Marshal(newBody)
	if err != nil {
		log.Println("此处出错,", err)
		return
	}
	groupMember := c.Dc.GetGroupMember(msg.GroupId)
	for _, v := range groupMember {
		// 发送信息
		c.send(v.UserId, data)
		// 存储到数据库
		c.Dc.MessageAdd(databaseClient.Message{
			AccountA:  sender,
			AccountB:  v.UserId,
			Direction: true,
			Group:     msg.GroupId,
			Type:      int8(msg.Type),
			Body:      msg.Msg,
			Extra:     msg.Extra,
			SendTime:  time.Now().UnixNano() / 1e6,
		})
	}
}

func (c *Chat) dealSingleApply(apply *wsProto.Apply, sender string) {
	// 如果id不存在，这里直接返回不进行任何处理
	if !c.Dc.UserIdIsExist(apply.GetEnd()) {
		return
	}
	// 接受好友的话
	if apply.GetType() == 2 {
		//判断是否存在好友关系，不存在则添加好友
		if !c.Dc.ExistChum(sender, apply.GetEnd()) {
			c.Dc.AddChum(sender, apply.GetEnd(), true)
		}
	}
	//删除好友
	if apply.GetType() == 4 {
		//判断是否存在好友关系，存在则删除好友
		if c.Dc.ExistChum(sender, apply.GetEnd()) {
			c.Dc.DelChum(sender, apply.GetEnd())
		}

	}
	newApply := &wsProto.Body{
		Type: 2,
		Apply: &wsProto.Apply{
			End:    sender,
			Time:   time.Now().UnixNano() / 1e6,
			Type:   apply.Type,
			Reason: apply.Reason,
		},
	}
	data, err := tool.Marshal(newApply)
	if err != nil {
		log.Println("此处出错,", err)
		return
	}
	fmt.Println("触发了回送")
	c.send(apply.End, data)
	c.Dc.AddApply(sender, int8(apply.Type), apply.End, "", apply.Deal, apply.Reason)
}

func (c *Chat) dealGroupApply(apply *wsProto.Apply, sender string) {
	// 判断groupId是否存在，不存在直接return
	if !c.Dc.GroupIsExist(apply.GetGroupId()) {
		return
	}
	// 当属于拒绝和同意申请的时候进行判断
	if apply.GetType() == 2 || apply.GetType() == 3 {
		// 判断end和deal是否为存在userid，不存在就return
		if !c.Dc.UserIdIsExist(apply.GetEnd()) || !c.Dc.UserIdIsExist(apply.GetDeal()) {
			return
		}
	}

	// 同意加群
	if apply.GetType() == 2 {
		// 判断用户是否为群成员，如果不是群成员则在这里加入群聊
		if !c.Dc.IsInGroup(apply.GetGroupId(), apply.GetEnd()) {
			c.Dc.AddGroupMember(apply.GetGroupId(), apply.GetEnd())
		}
	}
	// 退群
	if apply.GetType() == 4 {
		// 判断用户是否为群成员，如果是群成员则在这里退出群聊
		if c.Dc.IsInGroup(apply.GetGroupId(), apply.GetEnd()) {
			c.Dc.DelGroupMember(apply.GetGroupId(), apply.GetEnd())
		}

	}
	newApply := &wsProto.Body{
		Type: 2,
		Apply: &wsProto.Apply{
			End:     sender,
			Time:    time.Now().UnixNano() / 1e6,
			Type:    apply.Type,
			Deal:    apply.Deal,
			GroupId: apply.GroupId,
			Reason:  apply.Reason,
		},
	}
	data, err := tool.Marshal(newApply)
	if err != nil {
		log.Println("此处出错,", err)
		return
	}
	// 当属于拒绝和同意申请的时候向单个用户写入信息
	if apply.GetType() == 2 || apply.GetType() == 3 {
		c.Dc.AddApply(sender, int8(apply.Type), apply.End, apply.GroupId, apply.Deal, apply.Reason)
	}
	// 所有请求均需要向管理员写入信息
	groupAdmin := c.Dc.GetGroupAdmin(apply.GroupId)
	for _, v := range groupAdmin {
		c.send(v.UserId, data)
		c.Dc.AddApply(sender, int8(apply.Type), v.UserId, apply.GroupId, apply.Deal, apply.Reason)
	}
}
