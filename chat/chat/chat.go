package chat

import (
	"IM-RPC/chat/flag"
	databaseClient "IM-RPC/database/client"

	"github.com/smallnest/rpcx/server"
)

type Chat struct {
	C  *Conn
	Dc *databaseClient.Client
	s  *server.Server
}

// 构造函数
func NewChat(s *server.Server) *Chat {
	return &Chat{
		C:  NewConn(),
		Dc: databaseClient.NewClient([]string{flag.EtcdAddr}, flag.BasePath),
		s:  s,
	}
}

// 关闭连接函数
func (c *Chat) Close() {
	c.Dc.Close()
}
