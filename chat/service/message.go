package service

import (
	"context"
	"net"

	"github.com/smallnest/rpcx/server"
	"github.com/smallnest/rpcx/share"
)

// 注册
func (c *Chat) Register(ctx context.Context, args string, reply *interface{}) error {
	c.C.Register(args, ctx.Value(server.RemoteConnContextKey).(net.Conn))
	return nil
}

// 取消注册
func (c *Chat) UnRegister(ctx context.Context, args string, reply *interface{}) error {
	c.C.UnRegister(args)
	return nil
}

func (c *Chat) Pump(ctx context.Context, args []byte, reply *interface{}) error {
	reqMeta := ctx.Value(share.ReqMetaDataKey).(map[string]string)
	c.C.Pump(args, reqMeta["id"])
	return nil
}
