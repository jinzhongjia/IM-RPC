package service

import (
	cc "IM-RPC/chat/chat"

	"github.com/smallnest/rpcx/server"
)

type Chat struct {
	C *cc.Chat
}

func NewChat(s *server.Server) *Chat {
	return &Chat{
		C: cc.NewChat(s),
	}
}
