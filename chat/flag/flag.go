package flag

import (
	"flag"
)

var (
	Addr     string //本机 地址
	EtcdAddr string //etcd 地址
	BasePath string //服务 前缀
)

func init() {
	flag.StringVar(&Addr, "addr", "localhost:8979", "server address")
	flag.StringVar(&EtcdAddr, "etcdAddr", "localhost:2379", "etcd address")
	flag.StringVar(&BasePath, "base", "/IM", "prefix path")
}
