package main

import (
	"IM-RPC/chat/flag"
	"IM-RPC/chat/service"
)

func main() {
	s := NewService()
	s.RegisterName("Chat", service.NewChat(s.s), "")
	// 最后关闭数据库连接
	defer s.s.Close()
	// 注册服务

	s.Serve("tcp", flag.Addr)
}
