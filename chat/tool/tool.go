package tool

import (
	"IM-RPC/public/wsProto"

	"google.golang.org/protobuf/proto"
)

func Marshal(body *wsProto.Body) ([]byte, error) {
	return proto.Marshal(body)
}

func UnMarshal(data []byte) (*wsProto.Body, error) {
	newBody := &wsProto.Body{}
	err := proto.Unmarshal(data, newBody)
	return newBody, err
}
