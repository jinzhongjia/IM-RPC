package client

import (
	"context"
	"log"

	etcd_client "github.com/rpcxio/rpcx-etcd/client"
	"github.com/smallnest/rpcx/client"
	"github.com/smallnest/rpcx/protocol"
	"github.com/smallnest/rpcx/share"
)

type Client struct {
	Xclient client.XClient
	Ch      chan *protocol.Message
}

func NewClient(EtcdAddr []string, BasePath string) *Client {
	ch := make(chan *protocol.Message)
	d, _ := etcd_client.NewEtcdV3Discovery(BasePath, ServicePath, EtcdAddr, true, nil)

	return &Client{
		Xclient: client.NewBidirectionalXClient(ServicePath, client.Failover, client.RoundRobin, d, client.DefaultOption, ch),
		Ch:      ch,
	}
}

func (c Client) Close() {
	c.Xclient.Close()
}

func (c Client) handle(id string, name string, args interface{}, reply interface{}) {
	ctx := context.WithValue(context.Background(), share.ReqMetaDataKey, map[string]string{"id": id})
	err := c.Xclient.Call(ctx, name, args, reply)
	if err != nil {
		log.Printf("failed to call: %v\n", err)
	}
}

// 在chat注册当前用户在线
func (c Client) Register(id string) {
	err := c.Xclient.Broadcast(context.Background(), "Register", id, nil)
	if err != nil {
		log.Printf("failed to call: %v\n", err)
	}
}

// 取消注册
func (c Client) UnRegister(id string) {
	err := c.Xclient.Broadcast(context.Background(), "UnRegister", id, nil)
	if err != nil {
		log.Printf("failed to call: %v\n", err)
	}
}

// 传输消息
func (c Client) Pump(data []byte, sender string) {
	c.handle(sender, "Pump", data, nil)
}

// //单独作为协程运行
// func (c Client) receive() {
// 	// for
// 	for msg := range c.Ch {
// 		id := msg.Metadata["id"]
// 		log.Println(id)

// 	}
// }
