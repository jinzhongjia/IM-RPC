package main

import (
	"IM-RPC/chat/flag"
	"log"
	"time"

	"github.com/rcrowley/go-metrics"
	"github.com/rpcxio/rpcx-etcd/serverplugin"
	"github.com/smallnest/rpcx/server"
)

type Service struct {
	s *server.Server
}

//构造函数
func NewService() *Service {
	s := server.NewServer()
	addRegistryPlugin(s, &flag.Addr, &flag.EtcdAddr, &flag.BasePath)
	return &Service{s}
}

//服务注册
func (s *Service) RegisterName(name string, rcvr interface{}, metadata string) error {
	return s.s.RegisterName(name, rcvr, metadata)
}

//服务启动
func (s *Service) Serve(network, address string) error {
	return s.s.Serve(network, address)
}

func addRegistryPlugin(s *server.Server, addr *string, etcdAddr *string, basePath *string) {
	r := &serverplugin.EtcdV3RegisterPlugin{
		ServiceAddress: "tcp@" + *addr,
		EtcdServers:    []string{*etcdAddr},
		BasePath:       *basePath,
		Metrics:        metrics.NewRegistry(),
		UpdateInterval: time.Minute,
	}
	err := r.Start()
	if err != nil {
		log.Fatal(err)
	}
	s.Plugins.Add(r)
}
