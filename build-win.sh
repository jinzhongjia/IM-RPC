#!/bin/bash

# 删除编译出来的exe
rm *.exe

# 编译database模块
go build -ldflags="-s -w" -o im-rpc-database.exe ./database

# 编译session模块
go build -ldflags="-s -w" -o im-rpc-session.exe ./session

# 编译login模块
go build -ldflags="-s -w" -o im-rpc-login.exe ./login

# 编译chat模块
go build -ldflags="-s -w" -o im-rpc-chat.exe ./chat

# 编译gateway模块
go build -ldflags="-s -w" -o im-rpc-gateway.exe ./gateway

# 编译router模块
go build -ldflags="-s -w" -o im-rpc-router.exe ./router