package note

import (
	databaseClient "IM-RPC/database/client"
)

func GetNoteIndex(dc *databaseClient.Client, userId string) []databaseClient.NoteIndex {
	return dc.GetNoteIndex(userId)
}

func GetNoteContent(dc *databaseClient.Client, id string) databaseClient.NoteContent {
	return dc.GetNoteContent(id)
}

func CreateNote(dc *databaseClient.Client, userId string) {
	dc.CreateNote(userId)
}

func UpdateNoteTitle(dc *databaseClient.Client, id string, title string) {
	dc.UpdateNoteTitle(id, title)
}

func UpdateNoteContent(dc *databaseClient.Client, id string, content string) {
	dc.UpdateNoteContent(id, content)
	updateNoteAbstract(dc, id, abstract(content))
}

func updateNoteAbstract(dc *databaseClient.Client, id string, abstract string) {
	dc.UpdateNoteAbstract(id, abstract)
}

func UpdateNotePasswd(dc *databaseClient.Client, id string, passwd string) {
	dc.UpdateNotePasswd(id, passwd)
}

func DelNote(dc *databaseClient.Client, id string) {
	dc.DelNote(id)
}
