package note

import (
	"IM-RPC/login/flag"
	"regexp"
	"strings"

	"github.com/russross/blackfriday/v2"
)

func abstract(md string) string {
	tmp1 := blackfriday.Run([]byte(md), blackfriday.WithNoExtensions())
	reg := regexp.MustCompile("<[^>]*>")
	tmp2 := reg.ReplaceAllString(string(tmp1), "")
	tmp3 := strings.ReplaceAll(tmp2, " ", "")
	tmp4 := strings.ReplaceAll(tmp3, "\n", "")
	tmp5 := []rune(tmp4)
	if len(tmp5) > flag.AbstractLen-3 {
		tmp5 = tmp5[:flag.AbstractLen-3]
		return string(tmp5) + "..."
	}
	return string(tmp5)
}
