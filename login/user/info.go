package user

import (
	databaseClient "IM-RPC/database/client"
)

func GetUserData(dc *databaseClient.Client, id string) databaseClient.UserData {
	data := dc.GetUserData(id)
	return data
}

func UpdateName(dc *databaseClient.Client, id string, name string) {
	dc.UpdateName(id, name)
}

func UpdateSex(dc *databaseClient.Client, id string, sex bool) {
	dc.UpdateSex(id, sex)
}

func UpdateSignature(dc *databaseClient.Client, id string, signature string) {
	dc.UpdateSignature(id, signature)
}

func UpdatePhone(dc *databaseClient.Client, id string, phone int64) {
	dc.UpdatePhone(id, phone)
}

func UpdateMail(dc *databaseClient.Client, id string, mail string) {
	dc.UpdateMail(id, mail)
}

func UpdateSite(dc *databaseClient.Client, id string, site string) {
	dc.UpdateSite(id, site)
}

func UpdateGithub(dc *databaseClient.Client, id string, github string) {
	dc.UpdateGithub(id, github)
}

func UpdateQq(dc *databaseClient.Client, id string, qq int64) {
	dc.UpdateQq(id, qq)
}

func UpdateWeibo(dc *databaseClient.Client, id string, weibo string) {
	dc.UpdateWeibo(id, weibo)
}
