package user

import (
	databaseClient "IM-RPC/database/client"
	ac "IM-RPC/login/account"
	"log"
	"regexp"

	"golang.org/x/crypto/bcrypt"
)

func UpdateAccount(dc *databaseClient.Client, id string, account string) bool {
	// 账号正则匹配
	n, _ := regexp.MatchString(ac.NameRegexp, account)
	if !n {
		return false
	}
	dc.UpdateAccount(id, account)
	return true
}

func UpdatePasswd(dc *databaseClient.Client, id string, passwd string) bool {
	// 密码正则匹配
	p, _ := regexp.MatchString(ac.PasswdRegexp, passwd)
	if !p {
		return false
	}
	hash, err := bcrypt.GenerateFromPassword([]byte(passwd), bcrypt.DefaultCost)
	if err != nil {
		log.Println("passwd encrypt failed! passwd: ", passwd)
		return false
	}
	dc.UpdatePasswd(id, string(hash))
	return true
}
