package client

func (c *Client) GetFileIndex(userId string) []File {
	var fileIndex []File
	c.handle("GetFileIndex", userId, &fileIndex)
	return fileIndex
}

func (c *Client) GetFileIndexById(id string) File {
	var fileIndex File
	c.handle("GetFileIndexById", id, &fileIndex)
	return fileIndex
}

func (c *Client) GetFileContent(id string) FileIndex {
	var fileContent FileIndex
	c.handle("GetFileContent", id, &fileContent)
	return fileContent
}

func (c *Client) CreateFile(userId string, name string, size int64, t string, url string) {
	c.handle("CreateFile", FileIndex{
		UserId: userId,
		Name:   name,
		Size:   size,
		Type:   t,
		URL:    url,
	}, nil)
}

func (c *Client) UpdateFileName(id string, name string) {
	c.handle("UpdateFileName", FileIndex{
		ID:   id,
		Name: name,
	}, nil)
}

func (c *Client) UpdateFileUrl(id string, url string) {
	c.handle("UpdateFileUrl", FileIndex{
		ID:  id,
		URL: url,
	}, nil)
}

func (c *Client) DelFile(id string) {
	c.handle("DelFile", id, nil)
}
