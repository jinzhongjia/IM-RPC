package client

import databaseClient "IM-RPC/database/client"

func (c *Client) GetChum(userA string, userB string) databaseClient.Chum {
	var chum databaseClient.Chum
	c.handle("GetChum", Chum{
		UserA: userA,
		UserB: userB,
	}, &chum)
	return chum
}

func (c *Client) GetAllChum(id string) []databaseClient.Chum {
	var allChum []databaseClient.Chum
	c.handle("GetAllChum", id, &allChum)
	return allChum
}
