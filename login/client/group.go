package client

func (c *Client) CreateGroup(name string, ownerId string) {
	c.handle("CreateGroup", Group{
		Name:  name,
		Owner: ownerId,
	}, nil)
}

func (c *Client) GetGroups(userId string) []GroupMember {
	var groups []GroupMember
	c.handle("GetGroups", userId, &groups)
	return groups
}

func (c *Client) GetGroupInformation(groupId string) Group {
	var groupData Group
	c.handle("GetGroupInformation", groupId, &groupData)
	return groupData
}
