package client

import dc "IM-RPC/database/client"

func (c *Client) GetNoteIndex(userId string) []dc.NoteIndex {
	var noteIndex []dc.NoteIndex
	c.handle("GetNoteIndex", userId, &noteIndex)
	return noteIndex
}

func (c *Client) GetNoteContent(id string) dc.NoteContent {
	var noteContent dc.NoteContent
	c.handle("GetNoteContent", id, &noteContent)
	return noteContent
}

func (c *Client) CreateNote(userId string) {
	c.handle("CreateNote", userId, nil)
}

func (c *Client) UpdateNoteTitle(id string, title string) {
	c.handle("UpdateNoteTitle", NoteIndex{
		ID:    id,
		Title: title,
	}, nil)
}

func (c *Client) UpdateNoteContent(id string, content string) {
	c.handle("UpdateNoteContent", NoteContent{
		ID:      id,
		Content: content,
	}, nil)
}

func (c *Client) UpdateNotePasswd(id string, passwd string) {
	c.handle("UpdateNotePasswd", NoteContent{
		ID:     id,
		Passwd: passwd,
	}, nil)
}

func (c *Client) DelNote(id string) {
	c.handle("DelNote", id, nil)
}
