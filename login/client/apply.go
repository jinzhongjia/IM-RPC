package client

import databaseClient "IM-RPC/database/client"

func (c *Client) RecentApply(id string, num int) []databaseClient.Apply {
	var recentApply []databaseClient.Apply
	c.handle("RecentApply", Recent{
		ID:  id,
		Num: num,
	}, &recentApply)
	return recentApply
}

func (c *Client) ApplyOver(id string) {
	c.handle("ApplyOver", id, nil)
}
