package client

import (
	"context"
	"log"

	etcd_client "github.com/rpcxio/rpcx-etcd/client"
	"github.com/smallnest/rpcx/client"
)

type Client struct {
	Xclient client.XClient
}

func NewClient(EtcdAddr []string, BasePath string) *Client {
	d, _ := etcd_client.NewEtcdV3Discovery(BasePath, ServicePath, EtcdAddr, true, nil)

	return &Client{
		Xclient: client.NewXClient(ServicePath, client.Failover, client.RoundRobin, d, client.DefaultOption),
	}
}

func (c Client) Close() {
	c.Xclient.Close()
}

func (c Client) handle(name string, args interface{}, reply interface{}) {
	err := c.Xclient.Call(context.Background(), name, args, reply)
	if err != nil {
		log.Printf("failed to call: %v\n", err)
	}
}
