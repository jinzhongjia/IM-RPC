package client

import (
	databaseClient "IM-RPC/database/client"
)

func (c *Client) RecentMsg(id string, num int) []databaseClient.Message {
	var recentMsg []databaseClient.Message
	c.handle("RecentMsg", Recent{
		ID:  id,
		Num: num,
	}, &recentMsg)
	return recentMsg
}
