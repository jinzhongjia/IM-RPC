package client

type LoginInfo struct {
	Account string `json:"Account"`
	Passwd  string `json:"Passwd,omitempty"`
}

type User struct {
	ID      string `json:"ID"`
	Account string `json:"Account,omitempty"`
	Passwd  string `json:"Passwd,omitempty"`
}

type Session struct {
	SessionId string
	Err       error
}

type UserData struct {
	ID         string `json:"ID"`                   //用户主键，使用分布式全局主键写入,这里需要和user主键相同
	Name       string `json:"Name"`                 //用户昵称
	Sex        bool   `json:"Sex"`                  //性别
	CreateTime int64  `json:"CreateTime,omitempty"` //账户创建时间
	LastTime   int64  `json:"LastTime,omitempty"`   //最后登录时间
	Signature  string `json:"Signature,omitempty"`  //个性签名
	Phone      int64  `json:"Phone,omitempty"`      //手机号
	Mail       string `json:"Mail,omitempty"`       //邮件
	Site       string `json:"Site,omitempty"`       //个人网站
	Github     string `json:"Github,omitempty"`     //github
	Qq         int64  `json:"Qq,omitempty"`         //QQ号
	Weibo      string `json:"Weibo,omitempty"`      //微博
}
type NoteIndex struct {
	ID         string `json:"ID"`                   //笔记id
	Title      string `json:"Title,omitempty"`      //标题
	Abstract   string `json:"Abstract,omitempty"`   //摘要
	UserId     string `json:"UserId,omitempty"`     //用户id
	CreateTime int64  `json:"CreateTime,omitempty"` //创建时间
}

type NoteContent struct {
	ID       string `json:"ID"`                 //笔记id
	Content  string `json:"Content,omitempty"`  //内容
	Passwd   string `json:"Passwd,omitempty"`   //访问密码
	LastTime int64  `json:"LastTime,omitempty"` //上次修改时间
}

type FileIndex struct {
	ID         string `json:"ID"`                   //文件id
	UserId     string `json:"UserId,omitempty"`     //所属用户id
	Name       string `json:"Name,omitempty"`       //文件名
	Size       int64  `json:"Size,omitempty"`       //文件大小
	Type       string `json:"Type,omitempty"`       //文件类型
	CreateTime int64  `json:"CreateTime,omitempty"` //创建时间
	URL        string `json:"URL,omitempty"`        //文件路径
	Passwd     string `json:"Passwd,omitempty"`     //访问密码
}

type File struct {
	ID         string `json:"ID"`                   //文件id
	UserId     string `json:"UserId,omitempty"`     //所属用户id
	Name       string `json:"Name,omitempty"`       //文件名
	Size       int64  `json:"Size,omitempty"`       //文件大小
	Type       string `json:"Type,omitempty"`       //文件类型
	CreateTime int64  `json:"CreateTime,omitempty"` //创建时间
}

type Recent struct {
	ID  string
	Num int
}

type Chum struct {
	UserA      string //用户a
	UserB      string //用户b
	Direction  bool   //当Direction为true时，表示usera为发送方
	CreateTime int64  //好友添加时间
}

type Group struct {
	ID           string //群组ID
	Name         string //群名
	HeadPic      int8   //群头像
	Owner        string //群拥有者
	Introduction string //群介绍
	Time         int64  //创建时间
}

type GroupMember struct {
	GroupId  string //群号
	UserId   string //用户id
	JoinTime int64  //加入群聊时间
}
