package client

func (c *Client) Login(account string, passwd string) (string, error) {
	var session Session
	c.handle("Login", LoginInfo{
		Account: account,
		Passwd:  passwd,
	}, &session)
	return session.SessionId, session.Err
}

func (c *Client) LogOut(id string) {
	c.handle("LogOut", id, nil)
}

func (c *Client) Register(account string, passwd string) bool {
	var result bool
	c.handle("Register", LoginInfo{
		Account: account,
		Passwd:  passwd,
	}, &result)
	return result
}

func (c *Client) Authorization(sessionId string) bool {
	var result bool
	c.handle("Authorization", sessionId, &result)
	return result
}

func (c *Client) StI(sessionId string) string {
	var result string
	c.handle("StI", sessionId, &result)
	return result
}

func (c *Client) Its(id string) string {
	var result string
	c.handle("Its", id, &result)
	return result
}
