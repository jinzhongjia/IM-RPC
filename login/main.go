package main

import (
	"IM-RPC/login/flag"
	"IM-RPC/login/service"
	"log"
	"time"

	"github.com/rcrowley/go-metrics"
	"github.com/rpcxio/rpcx-etcd/serverplugin"
	"github.com/smallnest/rpcx/server"
)

func main() {
	s := server.NewServer()
	addRegistryPlugin(s, &flag.Addr, &flag.EtcdAddr, &flag.BasePath)
	login := service.NewLogin()
	defer login.O.Close()
	s.RegisterName("Login", login, "")

	s.Serve("tcp", flag.Addr)
}

func addRegistryPlugin(s *server.Server, addr *string, etcdAddr *string, basePath *string) {
	r := &serverplugin.EtcdV3RegisterPlugin{
		ServiceAddress: "tcp@" + *addr,
		EtcdServers:    []string{*etcdAddr},
		BasePath:       *basePath,
		Metrics:        metrics.NewRegistry(),
		UpdateInterval: time.Minute,
	}
	err := r.Start()
	if err != nil {
		log.Fatal(err)
	}
	s.Plugins.Add(r)
}
