package file

import databaseClient "IM-RPC/database/client"

func GetFileIndex(dc *databaseClient.Client, userId string) []databaseClient.File {
	return dc.GetFileIndex(userId)
}

func GetFileIndexById(dc *databaseClient.Client, id string) databaseClient.File {
	return dc.GetFileIndexById(id)
}

func GetFileContent(dc *databaseClient.Client, id string) databaseClient.FileIndex {
	return dc.GetFileContent(id)
}

func CreateFile(dc *databaseClient.Client, userId string, name string, size int64, t string, url string) {
	dc.CreateFile(userId, name, size, t, url)
}

func UpdateFileName(dc *databaseClient.Client, id string, name string) {
	dc.UpdateFileName(id, name)
}

func UpdateFileUrl(dc *databaseClient.Client, id string, url string) {
	dc.UpdateFileUrl(id, url)
}

func DelFile(dc *databaseClient.Client, id string) {
	dc.DelFile(id)
}
