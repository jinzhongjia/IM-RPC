package object

import (
	"IM-RPC/database/client"
	"IM-RPC/login/file"
)

func (o *Object) GetFileIndex(userId string) []client.File {
	return file.GetFileIndex(o.Dc, userId)
}

func (o *Object) GetFileIndexById(id string) client.File {
	return file.GetFileIndexById(o.Dc, id)
}

func (o *Object) GetFileContent(id string) client.FileIndex {
	return file.GetFileContent(o.Dc, id)
}

func (o *Object) CreateFile(userId string, name string, size int64, t string, url string) {
	file.CreateFile(o.Dc, userId, name, size, t, url)
}

func (o *Object) UpdateFileName(id string, name string) {
	file.UpdateFileName(o.Dc, id, name)
}

func (o *Object) UpdateFileUrl(id string, url string) {
	file.UpdateFileUrl(o.Dc, id, url)
}

func (o *Object) DelFile(id string) {
	file.DelFile(o.Dc, id)
}
