package object

import (
	"IM-RPC/database/client"
	"IM-RPC/login/apply"
)

func (o *Object) RecentApply(id string, num int) []client.Apply {
	return apply.RecentApply(o.Dc, id, num)
}

func (o *Object) ApplyOver(id string) {
	apply.ApplyOver(o.Dc, id)
}
