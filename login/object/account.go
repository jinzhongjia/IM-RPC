package object

import ac "IM-RPC/login/account"

func (o *Object) Login(account string, passwd string) (string, error) {
	// sessionId,err:=
	return ac.Login(o.Dc, o.Sc, account, passwd)
}

func (o *Object) LogOut(id string) {
	ac.LogOut(o.Sc, id)
}

func (o *Object) Register(account string, passwd string) bool {
	return ac.Register(o.Dc, account, passwd)
}

func (o *Object) Authorization(sessionId string) bool {
	return ac.Authorization(o.Sc, sessionId)
}

func (o *Object) StI(sessionId string) string {
	return ac.StI(o.Sc, sessionId)
}

func (o *Object) Its(id string) string {
	return ac.Its(o.Sc, id)
}
