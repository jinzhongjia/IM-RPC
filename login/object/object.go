package object

import (
	databaseClient "IM-RPC/database/client"
	"IM-RPC/login/flag"
	sessionClient "IM-RPC/session/client"
)

type Object struct {
	Dc *databaseClient.Client
	Sc *sessionClient.Client
}

// 构造函数
func NewObject() *Object {
	return &Object{
		Sc: sessionClient.NewClient([]string{flag.EtcdAddr}, flag.BasePath),
		Dc: databaseClient.NewClient([]string{flag.EtcdAddr}, flag.BasePath),
	}
}

// 关闭函数
func (o *Object) Close() {
	o.Dc.Close()
	o.Sc.Close()
}
