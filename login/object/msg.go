package object

import (
	"IM-RPC/database/client"
	"IM-RPC/login/msg"
)

func (o *Object) RecentMsg(id string, num int) []client.Message {
	return msg.RecentMsg(o.Dc, id, num)
}
