package object

import (
	"IM-RPC/database/client"
	"IM-RPC/login/chum"
)

func (o *Object) GetChum(userA string, userB string) client.Chum {
	return chum.GetChum(o.Dc, userA, userB)
}

func (o *Object) GetAllChum(id string) []client.Chum {
	return chum.GetAllChum(o.Dc, id)
}
