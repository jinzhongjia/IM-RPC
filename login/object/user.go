package object

import (
	"IM-RPC/database/client"
	"IM-RPC/login/user"
)

func (o *Object) GetUserData(id string) client.UserData {
	return user.GetUserData(o.Dc, id)
}

func (o *Object) UpdateName(id string, name string) {
	user.UpdateName(o.Dc, id, name)
}

func (o *Object) UpdateSex(id string, sex bool) {
	user.UpdateSex(o.Dc, id, sex)
}

func (o *Object) UpdateSignature(id string, signature string) {
	user.UpdateSignature(o.Dc, id, signature)
}

func (o *Object) UpdatePhone(id string, phone int64) {
	user.UpdatePhone(o.Dc, id, phone)
}

func (o *Object) UpdateMail(id string, mail string) {
	user.UpdateMail(o.Dc, id, mail)
}

func (o *Object) UpdateSite(id string, site string) {
	user.UpdateSite(o.Dc, id, site)
}

func (o *Object) UpdateAccount(id string, account string) bool {
	return user.UpdateAccount(o.Dc, id, account)
}

func (o *Object) UpdatePasswd(id string, passwd string) bool {
	return user.UpdatePasswd(o.Dc, id, passwd)
}

func (o *Object) UpdateGithub(id string, github string) {
	user.UpdateGithub(o.Dc, id, github)
}

func (o *Object) UpdateQq(id string, qq int64) {
	user.UpdateQq(o.Dc, id, qq)
}

func (o *Object) UpdateWeibo(id string, weibo string) {
	user.UpdateWeibo(o.Dc, id, weibo)
}
