package object

import (
	"IM-RPC/database/client"
	"IM-RPC/login/note"
)

func (o *Object) GetNoteIndex(userId string) []client.NoteIndex {
	return note.GetNoteIndex(o.Dc, userId)
}

func (o *Object) GetNoteContent(id string) client.NoteContent {
	return note.GetNoteContent(o.Dc, id)
}

func (o *Object) CreateNote(userId string) {
	note.CreateNote(o.Dc, userId)
}

func (o *Object) UpdateNoteTitle(id string, title string) {
	note.UpdateNoteTitle(o.Dc, id, title)
}

func (o *Object) UpdateNoteContent(id string, content string) {
	note.UpdateNoteContent(o.Dc, id, content)
}

func (o *Object) UpdateNotePasswd(id string, passwd string) {
	note.UpdateNotePasswd(o.Dc, id, passwd)
}

func (o *Object) DelNote(id string) {
	note.DelNote(o.Dc, id)
}
