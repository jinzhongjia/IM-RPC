package object

import (
	"IM-RPC/database/client"
	"IM-RPC/login/group"
)

func (o *Object) CreateGroup(name string, ownerId string) {
	group.CreatGroup(o.Dc, name, ownerId)
}

func (o *Object) GetGroups(userId string) []client.GroupMember {
	return group.GetGroups(o.Dc, userId)
}

func (o *Object) GetGroupInformation(groupId string) client.Group {
	return group.GetGroupInformation(o.Dc, groupId)
}
