package flag

import "flag"

var (
	Addr          string //本机 地址
	EtcdAddr      string //etcd 地址
	BasePath      string //服务 前缀
	SessionLength int
	AbstractLen   int
)

func init() {
	flag.StringVar(&Addr, "addr", "localhost:8975", "server address")
	flag.StringVar(&EtcdAddr, "etcdAddr", "localhost:2379", "etcd address")
	flag.StringVar(&BasePath, "base", "/IM", "prefix path")
	flag.IntVar(&SessionLength, "sl", 32, "the length of generate sessionlength")
	flag.IntVar(&AbstractLen, "abstractLen", 18, "the length of abstract")
	flag.Parse()
}
