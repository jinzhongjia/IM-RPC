package account

import sessionClient "IM-RPC/session/client"

// 登出
func LogOut(sc *sessionClient.Client, id string) {
	account := sc.Get(id, "account")
	if account == "" {
		return
	}
	sc.Destroy(account)
	sc.Destroy(id)
}
