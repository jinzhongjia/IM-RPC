package account

import sessionClient "IM-RPC/session/client"

func Authorization(sc *sessionClient.Client, sessionId string) bool {
	return sc.Exist(sessionId)
}
