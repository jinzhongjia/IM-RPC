package account

import (
	databaseClient "IM-RPC/database/client"
	"IM-RPC/login/flag"
	sessionClient "IM-RPC/session/client"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"reflect"
	"regexp"

	"golang.org/x/crypto/bcrypt"
)

// 登录函数，传入账户和密码原始值
func Login(dc *databaseClient.Client, sc *sessionClient.Client, account string, passwd string) (string, error) {

	// 账号正则匹配
	n, _ := regexp.MatchString(NameRegexp, account)
	// 密码正则匹配
	p, _ := regexp.MatchString(PasswdRegexp, passwd)

	if !(n && p) {
		return "", errors.New("账号或者密码格式不正确！")
	}

	//判断用户是否存在
	user := dc.GetUser(account)
	if reflect.DeepEqual(user, databaseClient.User{}) {
		return "", errors.New("用户不存在！")
	}

	// 判断密码是否正确
	err := bcrypt.CompareHashAndPassword([]byte(user.Passwd), []byte(passwd))
	if err != nil {
		return "", errors.New("密码不正确！")
	}

	//尝试获取sessionid，判断是否已经登陆了
	session := sc.Get(user.ID, "sessionId")
	if session != "" {
		return session, nil
	}

	//更新登录时间
	dc.UpdateLastTime(user.ID)
	// 生成sessionId
	sessionId, _ := generateRandomString(flag.SessionLength)
	//从数据库获取用户信息
	userData := dc.GetUserData(user.ID)
	{
		sc.Set(user.ID, "sessionId", sessionId)
		sc.Set(sessionId, "account", user.Account)
		sc.Set(sessionId, "id", user.ID)

		sc.Set(sessionId, "name", userData.Name)
		sc.Set(sessionId, "sex", userData.Sex)
	}
	return sessionId, nil
}

// GenerateRandomBytes returns securely generated random bytes.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func generateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}
	return b, nil
}

// GenerateRandomString returns a URL-safe, base64 encoded
// securely generated random string.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func generateRandomString(s int) (string, error) {
	b, err := generateRandomBytes(s)
	return base64.URLEncoding.EncodeToString(b), err
}
