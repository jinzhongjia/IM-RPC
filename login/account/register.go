package account

import (
	databaseClient "IM-RPC/database/client"
	"log"
	"reflect"
	"regexp"

	"golang.org/x/crypto/bcrypt"
)

//注册函数，传入账户和原始密码
func Register(dc *databaseClient.Client, account string, passwd string) bool {

	// 账号正则匹配
	n, _ := regexp.MatchString(NameRegexp, account)
	// 密码正则匹配
	p, _ := regexp.MatchString(PasswdRegexp, passwd)

	if !(n && p) {
		return false
	}
	hash, err := bcrypt.GenerateFromPassword([]byte(passwd), bcrypt.DefaultCost)
	if err != nil {
		log.Println("passwd encrypt failed! passwd: ", passwd)
		return false
	}
	tmp := dc.GetUser(account)
	if !reflect.DeepEqual(tmp, databaseClient.User{}) {
		return false
	}
	dc.AddUser(account, string(hash))
	return true
}
