/*
登录、登出、注册
*/
package account

import sessionClient "IM-RPC/session/client"

const (
	// 设置用户名：4-10位，英文和数字混合
	NameRegexp = "^[a-zA-Z0-9_]{4,10}$"
	// 设置密码：英文数字（可为纯英文和纯数字），6-20位
	PasswdRegexp = "^[0-9A-Za-z]{6,20}$"
)

func StI(sc *sessionClient.Client, sessionId string) string {
	id := sc.Get(sessionId, "id")
	return id
}

func Its(sc *sessionClient.Client, id string) string {
	sessionId := sc.Get(id, "sessionId")
	return sessionId
}
