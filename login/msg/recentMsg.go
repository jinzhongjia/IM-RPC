package msg

import databaseClient "IM-RPC/database/client"

func RecentMsg(dc *databaseClient.Client, id string, num int) []databaseClient.Message {
	return dc.GetMessage(id, num)
}
