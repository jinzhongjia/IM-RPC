package apply

import (
	databaseClient "IM-RPC/database/client"
)

func RecentApply(dc *databaseClient.Client, id string, num int) []databaseClient.Apply {
	return dc.GetApply(id, num)
}

func ApplyOver(dc *databaseClient.Client, id string) {
	dc.ApplyOver(id)
}
