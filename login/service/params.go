package service

type LoginInfo struct {
	Account string
	Passwd  string
}

type Session struct {
	SessionId string
	Err       error
}

type User struct {
	ID      string
	Account string
	Passwd  string
}

type UserData struct {
	ID         string //用户主键，使用分布式全局主键写入,这里需要和user主键相同
	Name       string //用户昵称
	Sex        bool   //性别
	CreateTime int64  //账户创建时间
	LastTime   int64  //最后登录时间
	Signature  string //个性签名
	Phone      int64  //手机号
	Mail       string //邮件
	Site       string //个人网站
	Github     string //github
	Qq         int64  //QQ号
	Weibo      string //微博
}

type NoteIndex struct {
	ID         string `gorm:"primaryKey"` //笔记id
	Title      string //标题
	Abstract   string //摘要
	UserId     string //用户id
	CreateTime int64  //创建时间
}

type NoteContent struct {
	ID       string `gorm:"primaryKey"` //笔记id
	Content  string //内容
	Passwd   string //访问密码
	LastTime int64  //上次修改时间
}

type FileIndex struct {
	ID         string `gorm:"primaryKey"` //文件id
	UserId     string //所属用户id
	Name       string //文件名
	Size       int64  //文件大小
	Type       string //文件类型
	CreateTime int64  //创建时间
	URL        string //文件路径
	Passwd     string //访问密码
}

type File struct {
	ID         string `gorm:"primaryKey"` //文件id
	UserId     string //所属用户id
	Name       string //文件名
	Size       int64  //文件大小
	Type       string //文件类型
	CreateTime int64  //创建时间
}

type Recent struct {
	ID  string
	Num int
}

type Chum struct {
	UserA      string //用户a
	UserB      string //用户b
	Direction  bool   //当Direction为true时，表示usera为发送方
	CreateTime int64  //好友添加时间
}

type Group struct {
	ID           string //群组ID
	Name         string //群名
	HeadPic      int8   //群头像
	Owner        string //群拥有者
	Introduction string //群介绍
	Time         int64  //创建时间
}
