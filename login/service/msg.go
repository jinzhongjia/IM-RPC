package service

import (
	"IM-RPC/database/client"
	"context"
)

func (lo *Login) RecentMsg(ctx context.Context, args Recent, reply *[]client.Message) error {
	*reply = lo.O.RecentMsg(args.ID, args.Num)
	return nil
}
