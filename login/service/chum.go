package service

import (
	"IM-RPC/database/client"
	"context"
)

func (lo *Login) GetChum(ctx context.Context, args Chum, reply *client.Chum) error {
	*reply = lo.O.GetChum(args.UserA, args.UserB)
	return nil
}

func (lo *Login) GetAllChum(ctx context.Context, args string, reply *[]client.Chum) error {
	*reply = lo.O.GetAllChum(args)
	return nil
}
