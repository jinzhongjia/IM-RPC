package service

import (
	"IM-RPC/database/client"
	"context"
)

func (lo *Login) CreateGroup(ctx context.Context, args Group, reply *interface{}) error {
	lo.O.CreateGroup(args.Name, args.Owner)
	return nil
}

func (lo *Login) GetGroups(ctx context.Context, args string, reply *[]client.GroupMember) error {
	*reply = lo.O.GetGroups(args)
	return nil
}

func (lo *Login) GetGroupInformation(ctx context.Context, args string, reply *client.Group) error {
	*reply = lo.O.GetGroupInformation(args)
	return nil
}
