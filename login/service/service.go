package service

import "IM-RPC/login/object"

type Login struct {
	O *object.Object
}

func NewLogin() *Login {
	return &Login{
		O: object.NewObject(),
	}
}
