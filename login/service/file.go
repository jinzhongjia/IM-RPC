package service

import (
	"IM-RPC/database/client"
	"context"
)

func (lo *Login) GetFileIndex(ctx context.Context, args string, reply *[]client.File) error {
	*reply = lo.O.GetFileIndex(args)
	return nil
}

func (lo *Login) GetFileIndexById(ctx context.Context, args string, reply *client.File) error {
	*reply = lo.O.GetFileIndexById(args)
	return nil
}

func (lo *Login) GetFileContent(ctx context.Context, args string, reply *client.FileIndex) error {
	*reply = lo.O.GetFileContent(args)
	return nil
}

func (lo *Login) CreateFile(ctx context.Context, args client.FileIndex, reply *interface{}) error {
	lo.O.CreateFile(args.UserId, args.Name, args.Size, args.Type, args.URL)
	return nil
}

func (lo *Login) UpdateFileName(ctx context.Context, args client.FileIndex, reply *interface{}) error {
	lo.O.UpdateFileName(args.ID, args.Name)
	return nil
}

func (lo *Login) UpdateFileUrl(ctx context.Context, args client.FileIndex, reply *interface{}) error {
	lo.O.UpdateFileUrl(args.ID, args.URL)
	return nil
}

func (lo *Login) DelFile(ctx context.Context, args string, reply *interface{}) error {
	lo.O.DelFile(args)
	return nil
}
