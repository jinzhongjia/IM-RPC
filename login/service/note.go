package service

import (
	"IM-RPC/database/client"
	"context"
)

func (lo *Login) GetNoteIndex(ctx context.Context, args string, reply *[]client.NoteIndex) error {
	*reply = lo.O.GetNoteIndex(args)
	return nil
}

func (lo *Login) GetNoteContent(ctx context.Context, args string, reply *client.NoteContent) error {
	*reply = lo.O.GetNoteContent(args)
	return nil
}

func (lo *Login) CreateNote(ctx context.Context, userId string, reply *interface{}) error {
	lo.O.CreateNote(userId)
	return nil
}

func (lo *Login) UpdateNoteTitle(ctx context.Context, args NoteIndex, reply *interface{}) error {
	lo.O.UpdateNoteTitle(args.ID, args.Title)
	return nil
}

func (lo *Login) UpdateNoteContent(ctx context.Context, args NoteContent, reply *interface{}) error {
	lo.O.UpdateNoteContent(args.ID, args.Content)
	return nil
}

func (lo *Login) UpdateNotePasswd(ctx context.Context, args NoteContent, reply *interface{}) error {
	lo.O.UpdateNotePasswd(args.ID, args.Passwd)
	return nil
}

func (lo *Login) DelNote(ctx context.Context, args string, reply *interface{}) error {
	lo.O.DelNote(args)
	return nil
}
