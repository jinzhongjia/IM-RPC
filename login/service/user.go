package service

import (
	"context"
)

func (lo *Login) GetUserData(ctx context.Context, args string, reply *UserData) error {
	userData := lo.O.GetUserData(args)
	// fmt.Println(userData)
	reply.ID = userData.ID
	reply.Name = userData.Name
	reply.Sex = userData.Sex
	reply.CreateTime = userData.CreateTime
	reply.LastTime = userData.LastTime
	reply.Signature = userData.Signature
	reply.Phone = userData.Phone
	reply.Mail = userData.Mail
	reply.Site = userData.Site
	reply.Github = userData.Github
	reply.Qq = userData.Qq
	reply.Weibo = userData.Weibo
	return nil
}

func (lo *Login) UpdateName(ctx context.Context, args UserData, reply *interface{}) error {
	lo.O.UpdateName(args.ID, args.Name)
	return nil
}

func (lo *Login) UpdateSex(ctx context.Context, args UserData, reply *interface{}) error {
	lo.O.UpdateSex(args.ID, args.Sex)
	return nil
}

func (lo *Login) UpdateSignature(ctx context.Context, args UserData, reply *interface{}) error {
	lo.O.UpdateSignature(args.ID, args.Signature)
	return nil
}

func (lo *Login) UpdatePhone(ctx context.Context, args UserData, reply *interface{}) error {
	lo.O.UpdatePhone(args.ID, args.Phone)
	return nil
}

func (lo *Login) UpdateMail(ctx context.Context, args UserData, reply *interface{}) error {
	lo.O.UpdateMail(args.ID, args.Mail)
	return nil
}

func (lo *Login) UpdateSite(ctx context.Context, args UserData, reply *interface{}) error {
	lo.O.UpdateSite(args.ID, args.Site)
	return nil
}

func (lo *Login) UpdateAccount(ctx context.Context, args User, reply *bool) error {
	*reply = lo.O.UpdateAccount(args.ID, args.Account)
	return nil
}

func (lo *Login) UpdatePasswd(ctx context.Context, args User, reply *bool) error {
	*reply = lo.O.UpdatePasswd(args.ID, args.Passwd)
	return nil
}

func (lo *Login) UpdateGithub(ctx context.Context, args UserData, reply *interface{}) error {
	lo.O.UpdateGithub(args.ID, args.Github)
	return nil
}

func (lo *Login) UpdateQq(ctx context.Context, args UserData, reply *interface{}) error {
	lo.O.UpdateQq(args.ID, args.Qq)
	return nil
}

func (lo *Login) UpdateWeibo(ctx context.Context, args UserData, reply *interface{}) error {
	lo.O.UpdateWeibo(args.ID, args.Weibo)
	return nil
}
