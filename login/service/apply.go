package service

import (
	"IM-RPC/database/client"
	"context"
)

func (lo *Login) RecentApply(ctx context.Context, args Recent, reply *[]client.Apply) error {
	*reply = lo.O.RecentApply(args.ID, args.Num)
	return nil
}

func (lo *Login) ApplyOver(ctx context.Context, args string, reply *interface{}) error {
	lo.O.ApplyOver(args)
	return nil
}
