package service

import "context"

func (lo *Login) Login(ctx context.Context, args LoginInfo, reply *Session) error {
	reply.SessionId, reply.Err = lo.O.Login(args.Account, args.Passwd)
	return nil
}

func (lo *Login) LogOut(ctx context.Context, args string, reply *interface{}) error {
	lo.O.LogOut(args)
	return nil
}

func (lo *Login) Register(ctx context.Context, args LoginInfo, reply *bool) error {
	*reply = lo.O.Register(args.Account, args.Passwd)
	return nil
}

func (lo *Login) Authorization(ctx context.Context, args string, reply *bool) error {
	*reply = lo.O.Authorization(args)
	return nil
}

func (lo *Login) StI(ctx context.Context, args string, reply *string) error {
	*reply = lo.O.StI(args)
	return nil
}

func (lo *Login) Its(ctx context.Context, args string, reply *string) error {
	*reply = lo.O.Its(args)
	return nil
}
