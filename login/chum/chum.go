package chum

import databaseClient "IM-RPC/database/client"

func GetChum(dc *databaseClient.Client, userA string, userB string) databaseClient.Chum {
	return dc.GetChum(userA, userB)
}

func GetAllChum(dc *databaseClient.Client, id string) []databaseClient.Chum {
	return dc.GetAllChum(id)
}
