package group

import databaseClient "IM-RPC/database/client"

func CreatGroup(dc *databaseClient.Client, name string, ownerId string) {
	dc.CreateGroup(name, ownerId)
}

func GetGroups(dc *databaseClient.Client, userId string) []databaseClient.GroupMember {
	return dc.GetGroups(userId)
}

func GetGroupInformation(dc *databaseClient.Client, groupId string) databaseClient.Group {
	return dc.GetGroupInformation(groupId)
}
