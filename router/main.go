package main

import (
	"IM-RPC/router/discovery"
	"IM-RPC/router/flag"
	"IM-RPC/router/tool"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	gin.SetMode(gin.ReleaseMode)
	ser := discovery.NewServiceDiscovery([]string{flag.EtcdAddr})
	if err := ser.WatchService(flag.BasePath); err != nil {
		panic("Initialization listening error")
	}
	r := gin.Default()

	//处理跨域问题
	r.Use(tool.Cors)
	r.GET("/", func(c *gin.Context) {
		address := ser.WLS()
		if address == "" {
			c.String(http.StatusInternalServerError, "error, there is not available server!")
		} else {
			c.String(http.StatusOK, ser.WLS())
		}
	})
	fmt.Println("http://" + flag.Addr)
	r.Run(flag.Addr)
}
