package discovery

import (
	"IM-RPC/public/GatewayProto"
	"IM-RPC/router/status"
	"context"
	"errors"
	"log"
	"strings"
	"time"

	"github.com/golang/protobuf/proto"
	clientv3 "go.etcd.io/etcd/client/v3"
)

//ServiceDiscovery 服务发现
type ServiceDiscovery struct {
	cli        *clientv3.Client //etcd client
	serverList *status.Ss       //服务列表
	// lock       sync.Mutex
}

//NewServiceDiscovery  新建发现服务
func NewServiceDiscovery(endpoints []string) *ServiceDiscovery {
	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   endpoints,
		DialTimeout: 5 * time.Second,
	})
	if err != nil {
		log.Fatal(err)
	}

	return &ServiceDiscovery{
		cli:        cli,
		serverList: status.Construct(),
	}
}

//WatchService 初始化服务列表和监视
func (s *ServiceDiscovery) WatchService(prefix string) error {
	//根据前缀获取现有的key
	resp, err := s.cli.Get(context.Background(), prefix, clientv3.WithPrefix())
	if err != nil {
		return err
	}

	for _, ev := range resp.Kvs {
		ss := &GatewayProto.Status{}
		//解码proto
		err = proto.Unmarshal(ev.Value, ss)
		if err != nil {
			return errors.New(
				strings.Join(
					[]string{
						"unmarshaling error:",
						err.Error(),
					},
					"",
				),
			)
		}
		// 添加
		s.serverList.Update(string(ev.Key), status.Status{
			ConNum: ss.GetConNum(),
			Weight: ss.GetWeight(),
		})
	}

	//监视前缀，修改变更的server
	go s.watcher(prefix)
	return nil
}

func (s *ServiceDiscovery) watcher(prefix string) {
	//最后关闭
	defer s.Close()

	rch := s.cli.Watch(context.Background(), prefix, clientv3.WithPrefix())
	log.Printf("watching prefix:%s now...", prefix)
	for wresp := range rch {
		for _, ev := range wresp.Events {

			// 对proto数据进行解码
			ss := &GatewayProto.Status{}
			//解码proto
			err := proto.Unmarshal(ev.Kv.Value, ss)

			// 判断解码是否出错
			if err != nil {
				log.Println("unmarshaling error:", err)
				continue
			}
			switch ev.Type {
			// 匹配到添加事件
			case clientv3.EventTypePut:
				s.serverList.Update(string(ev.Kv.Key), status.Status{
					ConNum: ss.GetConNum(),
					Weight: ss.GetWeight(),
				})
				// 匹配到下线事件
			case clientv3.EventTypeDelete:
				s.serverList.Del(string(ev.Kv.Key))
			}

		}
	}
}

//Close 关闭服务
func (s *ServiceDiscovery) Close() error {
	return s.cli.Close()
}

// 调用加权最小连接数算法返回服务地址
func (s *ServiceDiscovery) WLS() string {
	return s.serverList.WLS()
}
