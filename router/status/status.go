package status

import (
	"IM-RPC/router/flag"

	"strings"
)

type Status struct {
	// Address string //服务地址
	ConNum int32 //连接数
	Weight int32 //权重
}

type Ss struct {
	s map[string]*Status
}

//构造函数，返回结构体指针
func Construct() *Ss {
	return &Ss{
		s: make(map[string]*Status),
	}
}

//增加函数
func (tmp *Ss) Update(address string, data Status) {
	if data.Weight <= 0 {
		panic("weight not set!")
	}
	tmp.s[address] = &data
}

//删除函数
func (tmp *Ss) Del(address string) {
	delete(tmp.s, address)
}

//Weighted Least-Connection，加权最小连接数算法
func (tmp Ss) WLS() string {
	var (
		result string
		n      int32 = -1
	)
	for address, status := range tmp.s {
		if (status.ConNum/status.Weight < n) || (n == -1) {
			n = status.ConNum / status.Weight
			result = address
		}
	}
	return strings.TrimLeft(result, flag.BasePath)
}
