## 简介

分布式im即时通讯系统，微服务架构，使用Rpcx、Gin、Gorm、Go-redis、Go-etcd开发

一共分为六部分模块，分别是Database、Session、Login、Chat、Gateway、Router

使用rpcx将系统各功能模块化处理，将业务逻辑进行解耦操作，实现分布式部署

**优势**：

* 解耦：系统内的服务很大程度上是分离的。因此，整个应用程序可以轻松构建，更改和扩展
* 组件化：微服务被视为可以轻松更换和升级的独立组件
* 业务能力：微服务非常简单，专注于单一功能
* 独立开发：所有微服务都可以根据各自的功能轻松开发
* 独立部署：根据他们所提供的服务，可以在任何应用中单独部署
* 故障隔离：即使应用中的一个服务不起作用，系统仍然继续运行

### Database模块

对数据操作进行封装，使用rpcx封装为服务端，对逻辑进行进一步抽象，供Login和Chat调用，实现代码复用

### Session模块

对redis操作进行封装，实现redis存储session功能，使用rpcx封装为服务端，供Login调用

### Login模块

http请求处理模块，用于处理使用http无状态请求，例如登录、注册、修改密码等，使用rpcx封装为服务端，供Gateway调用

### Chat模块

im系统核心处理模块，处理实时消息的接收和转发，实现逻辑上较为复杂的功能，比如单聊、群聊、系统消息、等，使用rpcx封装为服务端，供Gateway调用

### Gateway模块

im系统对外暴露模块，用于屏蔽外部的非法访问、过滤无效信息等，通过etcd进行服务注册，使用了加权最小连接数算法保证每台gateway的负载相对平均，与客户服务端进行websocket长连接

### Router模块

im系统的路由模块，使用etcd进行服务发现，监听gateway的连接数，即负载情况，返回给客户端负载相对低的gateway地址

## 部署

前置环境：redis、mysql、etcd

这里推荐使用docker部署，映射默认端口即可（建议映射到localhost，避免对外暴露），以下为docker命令

```bash
#部署mysql
docker run -itd --name mysql -p 127.0.0.1:3306:3306 -e MYSQL_ROOT_PASSWORD=123456 -e MYSQL_DATABASE=test mysql
#部署etcd
docker run -d -p 127.0.0.1:2379:2379 -p 127.0.0.1:2380:2380 -e ALLOW_NONE_AUTHENTICATION=yes  --name etcd bitnami/etcd
#部署redis
docker run -itd --name redis -p 127.0.0.1:6379:6379 redis
```

运行构建好的目标程序即可（构建完成后至少六个文件，如果想实现分布式部署方式则会有6个文件以上产生）

在linux端和windows端可以采用（linux使用systemctl）将程序以服务形式运行，关于程序命令行参数可以使用命令行参数-h查看

注意：**程序的启动顺序是：mysql、redis、etcd、构建好的二进制程序**

## 构建

以下为在windows平台构建方法：

### 目标平台为windows

直接运行项目目录的`build-win.sh`即可

### 目标平台为linux

需要使用前置Dockerfile构建镜像,docker文件名为：Dockerfile-usual,地址见：[https://jinzh.me/go交叉编译以及替代方案/#toc-2](https://jinzh.me/go交叉编译以及替代方案/#toc-2)

使用刚构建的镜像来build程序，指令如下：

```bash
docker run --rm -t -v d:\project\go\IM-RPC:/build gobuilder:latest
#g:\project\go\IM-RPC为项目所在目录
```

构建出来的目标文件均为可直接在目标平台执行的二进制文件（无环境依赖），且均采用upx进行了加壳压缩！
