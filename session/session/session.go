package session

import (
	Redis "IM-RPC/session/redis"
	"context"
	"log"
	"time"

	"github.com/go-redis/redis/v8"
)

var ctx = context.Background()

type Session struct {
	s    *redis.Client
	time time.Duration
}

func NewSession(expiration time.Duration) Session {
	return Session{
		s:    Redis.NewRedis(),
		time: expiration,
	}
}

//Set key-value
func (session Session) Set(sessionId string, key string, value interface{}) {
	session.s.HSet(ctx, sessionId, key, value)
	if !session.Expire(sessionId, session.time) {
		log.Println("update the time failed!")
	}
}

//Get key-value
func (session Session) Get(sessionId string, key string) string {
	data, err := session.s.HGet(ctx, sessionId, key).Result()
	if err != nil {
		log.Println("error: get failed", err)
		return ""
		//这里需要更改不使用panic
	}
	if !session.Expire(sessionId, session.time) {
		log.Println("update the time failed!")
	}
	return data
}

//Del key-value
func (session Session) Del(sessionId string, key string) {
	_, err := session.s.HDel(ctx, sessionId, key).Result()
	if err != nil {
		panic(err)
	}
	if !session.Expire(sessionId, session.time) {
		log.Println("update the time failed!")
	}
}

//Set validity period
func (session Session) Expire(sessionId string, expiration time.Duration) bool {
	res, err := session.s.Expire(ctx, sessionId, expiration).Result()
	if err != nil {
		panic(err)
	}
	return res
}

//Set validity period
func (session Session) ExpireAt(sessionId string, time time.Time) bool {
	res, err := session.s.ExpireAt(ctx, sessionId, time).Result()
	if err != nil {
		panic(err)
	}
	return res
}

//Determines whether the element exists
func (session Session) KeyExist(sessionId string, key string) bool {
	res, err := session.s.HExists(ctx, sessionId, key).Result()
	if err != nil {
		panic(err)
	}
	return res
}

//Checks whether the cache entry exists
func (session Session) Exist(sessionId string) bool {
	res, err := session.s.Exists(ctx, sessionId).Result()
	if err != nil {
		panic(err)
	}
	if res > 0 {
		return true
	} else {
		return false
	}
}

//Delete the session
func (session Session) Destroy(sessionId string) bool {
	res, err := session.s.Del(ctx, sessionId).Result()
	if err != nil {
		panic(err)
	}
	if res > 0 {
		return true
	} else {
		return false
	}
}

//Count the session
func (session Session) Count() int64 {
	num, err := session.s.DBSize(ctx).Result()
	if err != nil {
		panic(err)
	}
	return num
}
