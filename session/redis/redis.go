package redis

import (
	"IM-RPC/session/flag"
	"context"
	"fmt"

	"github.com/go-redis/redis/v8"
)

func NewRedis() *redis.Client {
	redis := redis.NewClient(&redis.Options{
		Addr:     flag.RedisAddr,
		Password: flag.RedisPasswd,
		DB:       1,
		//这里使用redis的1号库作为session存储
	})

	_, err := redis.Ping(context.Background()).Result()
	if err != nil {
		fmt.Printf("连接redis出错，错误信息：%v", err)
	}
	return redis
}
