package flag

import (
	"flag"
)

var (
	Addr        string //本机 地址
	EtcdAddr    string //etcd 地址
	BasePath    string //服务 前缀
	RedisAddr   string //redis 地址
	RedisPasswd string //redis 密码
	Expiration  int    //session 存活时间，分
)

func init() {
	flag.StringVar(&Addr, "addr", "localhost:8972", "server address")
	flag.StringVar(&EtcdAddr, "etcdAddr", "localhost:2379", "etcd address")
	flag.StringVar(&BasePath, "base", "/IM", "prefix path")
	flag.StringVar(&RedisAddr, "redisAddr", "localhost:6379", "redis address")
	flag.StringVar(&RedisPasswd, "redisPasswd", "", "redis passwd")
	flag.IntVar(&Expiration, "expiration", 90, "session expiration")
	flag.Parse()
}
