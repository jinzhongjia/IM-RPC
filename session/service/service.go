package service

import (
	"IM-RPC/session/flag"
	ss "IM-RPC/session/session"
	"context"
	"time"
)

type Args struct {
	SessionId string
	Key       string
	Value     interface{}
}

type Session struct {
	ss ss.Session
}

func NewSession() Session {
	return Session{
		ss: ss.NewSession(time.Duration(flag.Expiration) * time.Minute),
	}
}

func (session Session) Set(ctx context.Context, args Args, reply *interface{}) error {
	session.ss.Set(args.SessionId, args.Key, args.Value)
	return nil
}

func (session Session) Get(ctx context.Context, args Args, reply *string) error {
	*reply = session.ss.Get(args.SessionId, args.Key)
	return nil
}

func (session Session) Del(ctx context.Context, args Args, reply *interface{}) error {
	session.ss.Del(args.SessionId, args.Key)
	return nil
}

func (session Session) Exist(ctx context.Context, args string, reply *bool) error {
	*reply = session.ss.Exist(args)
	return nil
}

func (session Session) KeyExist(ctx context.Context, args Args, reply *bool) error {
	*reply = session.ss.KeyExist(args.SessionId, args.Key)
	return nil
}

func (session Session) Destroy(ctx context.Context, args string, reply *bool) error {
	*reply = session.ss.Destroy(args)
	return nil
}

func (session Session) Count(ctx context.Context, args interface{}, reply *int64) error {
	*reply = session.ss.Count()
	return nil
}
