package client

type Args struct {
	SessionId string
	Key       string
	Value     interface{}
}
