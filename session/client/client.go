package client

import (
	"context"
	"log"

	etcd_client "github.com/rpcxio/rpcx-etcd/client"
	"github.com/smallnest/rpcx/client"
)

type Client struct {
	Xclient client.XClient
}

func NewClient(EtcdAddr []string, BasePath string) *Client {
	d, _ := etcd_client.NewEtcdV3Discovery(BasePath, ServicePath, EtcdAddr, true, nil)

	return &Client{
		Xclient: client.NewXClient(ServicePath, client.Failover, client.RoundRobin, d, client.DefaultOption),
	}
}

func (c Client) Close() {
	c.Xclient.Close()
}

func (c Client) handle(name string, args interface{}, reply interface{}) {
	err := c.Xclient.Call(context.Background(), name, args, reply)
	if err != nil {
		log.Printf("failed to call: %v\n", err)
	}
}

func (c Client) Set(sessionId string, key string, value interface{}) {
	args := &Args{
		SessionId: sessionId,
		Key:       key,
		Value:     value,
	}
	c.handle("Set", args, nil)
}

func (c Client) Get(sessionId string, key string) string {
	var reply string
	args := &Args{
		SessionId: sessionId,
		Key:       key,
	}
	c.handle("Get", args, &reply)
	return reply
}

func (c Client) Del(sessionId string, key string) {
	args := &Args{
		SessionId: sessionId,
		Key:       key,
	}
	c.handle("Del", args, nil)
}

func (c Client) Exist(sessionId string) bool {
	var result bool
	c.handle("Exist", sessionId, &result)
	return result
}

func (c Client) KeyExist(sessionId string, key string) bool {
	var result bool
	args := &Args{
		SessionId: sessionId,
		Key:       key,
	}
	c.handle("KeyExist", args, &result)
	return result
}

func (c Client) Destroy(sessionId string) bool {
	var result bool
	c.handle("Destroy", sessionId, &result)
	return result
}

func (c Client) Count() int64 {
	var result int64
	c.handle("Count", nil, &result)
	return result
}
